Intro til IT-Sikkerhed

Læringsmål

# Noter
## ip address
255.255.255.0 er en subnet mask. Der kan være 254 hosts på dette netværk fordi 2 addresser er reseveret af routeren og broadcast.  

## porte
Første 1000 re reseveret til forskellige standarder
80 til http
443 til https
22 til ssh

reverse shell
reverse proxy


# Uge 6

## Opgaver

### Opgave 24
https://ucl-pba-its.gitlab.io/exercises/intro/24_intro_opgave_vmware_kali/#information

Vi skulle download vmware og kære kali linux på den. Der var nogle premade kali VMs man kunne bruge så og jeg valgte at bruge den. Default bruger navn og password var kali. 

### Opgave 25
Efter det pingede jeg 8.8.8.8 for at se hvis jeg havde internet forbindelse. Derefter gik jeg ind i THM (https://tryhackme.com/access?o=vpn) og downloaded opvpn config filen. Jeg kørte kommandoen  
`openvpn erwe26710`  
Det virkede ikke og jeg hørte rundt om klassen at VPN virkede ikke så jeg gave hurtig op og brugte attackbox i stedet. Jeg gik ind i tutorial rummet (https://tryhackme.com/room/tutorial) og startede attackboxen og target maskinen. Jeg fulgte guiden og indtastede ip adressen i firefox og fik tekst strengen. Efter jeg blev færdig med opgaven fik jeg at vide at VPN virkede. Jeg kiggede på guiden til hvordan man forbinder til den og så at jeg manglede `sudo` foran kommandoen.

### THM
#### Bookface hack

Jeg skulle præve at hacke i en bookface profil. Man kan finde user name ved at kigge op i URL. Når man har en user name kan man lave en password reset og brute force password reset koden man får i sin email.

# Uge 7

### Opgave 28
https://ucl-pba-its.gitlab.io/exercises/intro/28_intro_opgave_cia/

C - Confidentiality/Fortrolighed
I - Integrity/Integritet
A - Availibility/Tilgænglighed


For en energileverandør tilgænglighed er meget vigtig. Vi bruger enheder der kræver strøm f.eks. computer hver dag. Der er også andre kritiske ting der har også brug for strøm så jeg synes at availability er den mest vigtig for en energileverandør.
Jeg ved ikke hvor meget af en energileverandørs infrastruktur ligger online men en DDOS angreb vil være skadelig fordi den kan lukke en service ned.  

### THM: Introductory networking
https://tryhackme.com/room/introtonetworking  

Opgaverne handlede om OSI og TCP/IP modeller. Der var mange spørgsmål der sprugte f.eks. hvilket lag ad OSi modellen er ansvarlig for x.
Bagefter skulle jeg lære at bruge man, ping, traceroute, whois, og dig kommandoer.

![OSIModelAnOverview1](images/OSIModelAnOverview1.png)  
![OSIModelAnOverview2](images/OSIModelAnOverview2.png)  
![TCPIPModel](images/TCPIPModel.png)  
![ping](images/ping.png)  
![PingICMP](images/PingICMP.png)  
![Traceroute](images/Traceroute.png)  
![WHOIS](images/WHOIS.png)  
![dig](images/dig.png)  

### THM: What is networking
https://tryhackme.com/room/whatisnetworking  

Her lære jeg om netværk, ip, og MAC.

![WhatIsNetworking](images/WhatIsNetworking.png)  

### THM: Intro to LAN
https://tryhackme.com/room/introtolan  

LAN topologier, subnetting, ARP, DHCP
![IntroducingLANTopologies](images/IntroducingLANTopologies.png)  
![APrimerOnSubnetting](images/APrimerOnSubnetting.png)  
![TheARPProtocol](images/TheARPProtocol.png)  
![TheDHCPProtocol](images/TheDHCPProtocol.png)  

# Uge 8  

### THM: Nmap
https://tryhackme.com/room/furthernmap  

![NMAPIntro](images/NMAPIntro.png)  
![NMAPSwitches1](images/NMAPSwitches1.png)  
![NMAPSwitches2](images/NMAPSwitches2.png)  
![NMAPTCPScan](images/NMAPTCPScan.png)  
![NMAPSYNScan](images/NMAPSYNScan.png)  
![NMAPUDPScan](images/NMAPUDPScan.png)  
![NMAPNULLFINXMAS](images/NMAPNULLFINXMAS.png)  
![NMAPICMPScan](images/NMAPICMPScan.png)  
![NSEScriptsOverview](images/NSEScriptsOverview.png)  
![WorkingWithNSE](images/WorkingWithNSE.png)  
Fandte svaret ved at google ftp-anon.nse og fandte  
nmap.org/nsedoc/scripts/ftp-anon.html
![SearchingForNSEScripts](images/SearchijngForNSEScripts.png)  
Fandte svaret med den nedenstående kommando  
![lsScriptsSMB](images/lsScriptsSMB.png)  
dependency  
![smbOSDependency](images/smbOSDependency.png)  

![FirewallEvasion](images/FirewallEvasion.png)  

Kunne ikke få XMAS scan til at virke  
![xmasScan](images/xmasScan.png)  
TCPSYNScna virker heller ikke  
![TCPSYNScan](images/TCPSYNScan.png)  
virker ikke  
![ftpAnonScripts](images/ftpAnonScript.png)  

Har ikke fundet ud af hvorfor jeg får fejlen "Host seems down. If it is really up, but blocking out ping probes, try -Pn"  
Jeg har prøvet nmap -Pn [Machine_IP] men der sker intet  

### THM: Wireshark: The Basics
https://tryhackme.com/room/wiresharkthebasics  
![WiresharkXML](images/WiresharkXML.png)  
Arrival date kan findes hvis man udvider "Frame 38"
![WiresharkArrivalDate](images/WiresharkArrivalDate.png)  
Kan findes hvis man udvider "Internet Protocol Version 4"
![WiresharkTTL](images/WiresharkTTL.png)  
Kan findes hvis man udvider "Transmission Control Protocol"  
![WiresharkTCPPayloadSize](images/WiresharkTCPPayloadSize.png)  
Kan findes hvis man udvider "Hyper Text Transfer Protocol"  
![WiresharkEtag](images/WiresharkEtag.png)  
![WiresharkPacketDissection](images/WiresharkPacketDissection.png)  
Tryk på Edit derefter "Find Packet". Skrive r4w i søgefeltet
![WiresharkFindPacket](images/WiresharkFindPacket.png)  
![WiresharkArtist1](images/WiresharkArtist1.png) 
Højreklik på packet 12 og tryk på "packet comment" find packet 39765 og højreklik på "JPEG File Interchange Format" og tryk på "Export packet bytes"
![WiresharkJPEG](images/WiresharkJPEG.png)  
Gemte billedet og brygte kommandoen "md5sum JEPG" og fik MD5 hash  
![JPEGmd5sum](images/JPEGmd5sum.png)  
Mens "JPEG File Interchange Format" var selected trykkede jeg på File også "Export objects". Jeg fandte en fil der hed note.txt og trykkede på save.  
![AlienName](images/PacketMaster.png)  
Der var noget line art hvor der stod PACKETMAST. Jeg kunne gætte at der gik noget galt og der skulle stå PACKETMASTER. 
Tryk på Analyze også "Expert information" 
![WiresharkWarning](images/WiresharkWarning.png)  
![WiresharkPacketNavigation](images/WiresharkPacketNavigation.png)  
Find packet 4 og apply as filter  
![WiresharkFilter](images/WiresharkFilter.png)  
Man kan finde mængde af packets der bliver vist i bunden af aplikationen  
![WiresharkDisplayedPackets](images/WiresharkDisplayedPackets.png)  
Jeg fandte packet 33790, ekspoterede objektet og gemte artists.php. Jeg tænkte hvis jeg skulle finde informatiopn om kunstner så var det nok i denne fil.  
![WiresharkArtists](images/WiresharkArtists.png)  
![WiresharkTCPPayloadSize](images/WiresharkPacketFiltering.png)  

### Opgave 26
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187711  

1) What are the host names of the 3 Windows hosts from the pcap?  
&nbsp;&nbsp;&nbsp;&nbsp;192.168.204.139 == ROCKETMAN-PC  
&nbsp;&nbsp;&nbsp;&nbsp;192.168.204.137 == MYHUMPS-PC  
&nbsp;&nbsp;&nbsp;&nbsp;192.168.204.146 == WORKSTATION6  
2) What is(are) the IP address(es) of the Windows host(s) that hit an exploit kit?
3) What is(are) the MAC address(es) of the Windows host(s) that hit an exploit kit?
4) What is(are) the domain name(s) of the compromised web site(s)?
5) What is(are) the IP address(es) of the compromised web site(s)?
6) What is(are) the domain name(s) for the exploit kit(s)?
7) What is(are) the IP address(es) for the exploit kit(s)?
8) Did any of these hosts get infected?  If so, which host(s)? 

Første lagde mærke til 3 ip addresser  
192.168.204.139
192.168.204.137
192.168.204.146

Højreklikede på den første som er .139 og apply filter. Dobbeltklikkede på en TCP forbindelse og så at under "Ethernet II" og der stod VMware_61 under source så jeg  
troede at forbindelsen kom fra en virtuelmaskin der hed VMware_61 men jeg kunne se i pdfen at det var forkerte. Jeg gik ud af packet inspection og så at der stod  
ROCKETMAN-PC i info. Derefter filtrerede jeg efter de andre to ip addresser og fandte deres navne.
![vmwareFejl](images/vmwareFejl.png)  
![ROCKETMANPC](images/ROCKETMANPC.png)  

### Opgave 16
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187704  

### Opgave 17
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187705  

### Opgave 18
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187706  

### Opgave 19
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187707  

### Opgave 20
https://ucl.itslearning.com/LearningToolElement/ViewLearningToolElement.aspx?LearningToolElementId=2187708  

problemet var 0/0/1 var adapter 1

Host unreachable problem. kan ikke pinge noget.

## Uge 11

### Opgave 11

![nmapScriptFailed](images/nmapScriptFailed.png)  

### Opgave 14
![PSVersion](images/PSVersion.png)  
![PSFile](images/PSFile.png)  
![PSFile2](images/PSFile2.png)  

### Opgave 27

Instruktioner

1. Undersøg hvor gammel den nuværende password politik hos UCL er (kig efter gamle microsoft password anbefalinger)  
&nbsp;&nbsp;&nbsp;&nbsp;Password politik https://ucldk.4me.com/self-service/requests/new/show_knowledge_article?from=search&knowledge_article_id=85906&requested_for_id=4324720&search_id=13442183&position=2
&nbsp;&nbsp;&nbsp;&nbsp;Den eneste jeg kunne finde om gamle microsoft password anbefalinger er
![MicrosoftServer2012Password](images/MicrosoftServer2012Password.png)  
fra https://youtu.be/buZewCeg_cY?t=147
2. Debatter om hvilke konsekvenser IT politikker har for miljøet og virksomheder ressource forbrug.  
&nbsp;&nbsp;&nbsp;&nbsp;Hvis password politik er for streng så kræver det flere ressourcer for at velighold den. Men hvis password politikken er ikke  
&nbsp;&nbsp;&nbsp;&nbsp;streng nok så formindske det sikkerheden.
3. Snak om andre områder indenfor IT som i kan påvirke i forhold til miljør og ressourcer.  
&nbsp;&nbsp;&nbsp;&nbsp;Alle områder indenfor it kræver et eller andet form for ressource f.eks. plads eller strøm  
4. Undersøg hvad anbefalingerne til password politik er i 2022. Tag udgangspunkt i https://learn.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations og find selv flere kilder.  
&nbsp;&nbsp;&nbsp;&nbsp;https://support.google.com/accounts/answer/32040?hl=en#zippy=
![GooglePassPolicy1](images/GooglePassPolicy1.png)  
![GooglePassPolicy1](images/GooglePassPolicy2.png)  
![GooglePassPolicy1](images/GooglePassPolicy3.png)  
![GooglePassPolicy1](images/GooglePassPolicy4.png)  
![GooglePassPolicy1](images/GooglePassPolicy5.png)  
5. Nævner ISO27000 eller andre standarder noget om password politik ?  
6. Skriv et forslag til en opdateret miljø og ressource besparende password politik (i må gerne tænke alternativt i f.eks yubikey eller MFA) og giv jeres bedste bud på hvad det kan spare miljøet og UCL for i ressourcer.  
https://www.microsoft.com/en-us/security/blog/2022/05/31/secure-access-for-a-connected-worldmeet-microsoft-entra/  
Microsoft Entra er Microsofts forsøg på at fjerne passwords. Hvis jeg har forståede det rigtig så bruger man et id til autenfificering.

### Opgave 12  

![Socket1](images/Socket1.png)  
![Socket1Wireshark](images/Socket1Wireshark.png)  


1. Læs om pythons socket bibliotek https://docs.python.org/3/library/socket.html  

2. Læs koden i socket_1.py Hvad gør koden og hvordan bruger den socket biblioteket?  

3. Kør koden med default url og observer hvad der sker, du kan også prøve at single steppe filen for at kunne følge med i variabler osv. https://python.land/creating-python-programs/python-in-vscode  

4. Åben wireshark (installer det hvis du ikke allerede har det)  

5. Kør koden igen med default url, analyser med wireshark og besvar følgende:  

&nbsp;&nbsp;&nbsp;&nbsp;a. Hvad er destinations ip ?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;104.18.22.19  

&nbsp;&nbsp;&nbsp;&nbsp;b. Hvilke protokoller benyttes ?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TCP og HTTP  

&nbsp;&nbsp;&nbsp;&nbsp;c. Hvilken content-type bruges i http header ?  
![Socket1WiresharkCT](images/Socket1WiresharkCT.png)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Text/HTML

&nbsp;&nbsp;&nbsp;&nbsp;d. Er data krypteret ?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Der står HTTP så den er ikke krypterede.  

&nbsp;&nbsp;&nbsp;&nbsp;e. Hvilken iso standard handler dokumentet om ?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iso8859-1 der handler om ASCII encoding

6. Læs socket_2.py og undersøg hvordan linje 38 - 47 kan tælle antal modtagne karakterer  

7. Kør socket_2.py og prøv at hente forskellige url's der benytter https, hvad modtager du, hvilken http response får du, hvad betyder de og er de krypteret ?  
![Socket2Google](images/Socket2Google.png)  
&nbsp;&nbsp;&nbsp;&nbsp;Der står HTTP så den er ikke krypterede. 

8. Gentag trin 7 og analyser med wireshark for at finde de samme informationer der  
![Socket2GoogleWireshark](images/Socket2GoogleWireshark.png)  

9. Åben socket_3.py, analyser koden og omskriv den så kun http headers udskrives.  
![Socket3Headers](images/Socket3Headers.png)  
![Socket3HeadersOutput](images/Socket3HeadersOutput.png)  

###urllib

Læs om urllib biblioteket https://docs.python.org/3/library/urllib.html  
&nbsp;&nbsp;&nbsp;&nbsp;Åbn urllib_1.py og læs koden, hvordan er syntaksen ifht. socket 1-3 programmerne?  
&nbsp;&nbsp;&nbsp;&nbsp;Hvilket datasvar får du retur hvis du prøver at hente via https, f.eks https://docs.python.org/3/library/urllib.html  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jeg får sidens html

&nbsp;&nbsp;&nbsp;&nbsp;Kør programmet igen med https://docs.python.org/3/library/urllib.html og analyser i wireshark, besvar følgende:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hvad er destinations ip?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;199.232.40.223  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hvilke protokoller benyttes?  
![URLLIBWireshark](images/URLLIBWireshark.png)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TCP og DNS  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hvilken content-type bruges i http(s) header?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Der var ikke noget HTTP?  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Er data krypteret?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Der stod TLS så ja, den er krypterede.  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hvor mange cipher suites tilbydes i Client hello?  
![URLLIBCipherSuites](images/URLLIBCipherSuites.png)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;91  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hvilken TLS version og cipher suite bliver valgt i Server Hello?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Der står TLSv1.3 og TLSv1.2 så jeg ikke hvilket version den bruger begge to måske?  


###BeautifulSoup

&nbsp;&nbsp;&nbsp;&nbsp;Læs om BeatifulSoup biblioteket https://beautiful-soup-4.readthedocs.io/en/latest/  
&nbsp;&nbsp;&nbsp;&nbsp;Hvad er formået med biblioteket?  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hente data fra HTML og XML.  

&nbsp;&nbsp;&nbsp;&nbsp;Åbn urllib_2.py og analyser koden for at finde ud af hvad den gør.  
&nbsp;&nbsp;&nbsp;&nbsp;Kør programmet  
![URLLIB2Output](images/URLLIB2Output.png)  

&nbsp;&nbsp;&nbsp;&nbsp;Ret i programmet så det tæller et andet HTML tag  
![URLLIB2ChangedTag](images/URLLIB2ChangedTag.png)  
![URLLIB2ChangedTagOutput](images/URLLIB2ChangedTagOutput.png)  

&nbsp;&nbsp;&nbsp;&nbsp;Ret i programmet så det bruger BeatifulSoups .findall() metode  
![URLLIB2FindAll](images/URLLIB2FindAll.png)  

###Feedparser

&nbsp;&nbsp;&nbsp;&nbsp;Læs om feedparser biblioteket https://feedparser.readthedocs.io/en/latest/
&nbsp;&nbsp;&nbsp;&nbsp;Åbn rssfeed_parse.py og analyser koden for at finde ud af hvad den gør.
&nbsp;&nbsp;&nbsp;&nbsp;Linje 28, som formatterer modtaget data, har en meget lang sætning.
&nbsp;&nbsp;&nbsp;&nbsp;rssfeed_parse.py

f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n'

Hvad gør den ? ( du kan prøve at erstatte den med {entry.summary} for at se forskellen)

Find et passende sikkerheds relateret rss feed, på internettet, du vil parse.

Omskriv rssfeed_parse.py til at bruge dit valgte feed, ret i formatteringen, så du får et output i markdown filen, der svarer til:

# Headline/title

**_Author:_**
**_Summary:_**
[link_to_full_article](link_to_full_article)