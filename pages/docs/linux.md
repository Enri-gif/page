#Kommandoer

pwd - print working directory - den printer stien du er i.  
cd - change directory - den flytter brugeren til en anden mappe  
ls - list - den printer en inholdet af mapen brugeren er i  
touch - opretter en tom plain tekstfil  
mkdir - make directory - opretter en mappe  
man - manual - se en manual for en kommando  
find - søg  
truncate - tvinger en fil til at være en bestemt størrelse  
cp - copy  
mv - move  
rm - remove  
echo - print noget til terminalen  
cat - concatonate - print inholdet af en tekstfil til terminalen  
grep - filtere noget  
adduser - tilføj en bruger til systemet  
su - login som super user  
ps - se processer der kører  

#Tegn
~ - Home  
. - hvor brugeren er.  
.. - går en op i stien  
/ - rod  
."filnavn" - skjulte fil  

#Fil struktur
/ - rod - alle filer i et linux system ligger i rodden  
bin - binary - hvor essentielle binær ligger  
boot - bootloader filer  
dev - device - hvor enheder ligger  
etc - configurations filer  
home - hvor brugerens filer ligger  
media - fjernbar enheder f.eks. extern hardisk  
lib - libraries og kernal modules  
mnt - midlertidig mounted enheder  
misc -  
proc - information om processer der kører  
opt - addons for software   
sbin - system binaries  
tmp - midlertidig filer der bliver slettede efter systemet er færdig med startup  
usr - brugerens programmer  
var - variabler  
