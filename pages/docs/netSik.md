netværk

![InternetSpeedPC](images/InternetHastighedPC.png)  
#Noter

Router  
router mellem to subnets

switch  
Facilitere forbindelser mellem enheder


subnets  
ip address range

subnet masks  
/24 2^8-2

DHCP

Default gateway

CPE  
https://en.wikipedia.org/wiki/Customer-premises_equipment

VLAN

Tagged/untagged

Fysiske/Logiske netværk

cipr

headless server

ids

tls/ssl

CA

Symmatrisk/Asymmatrisk kryptering

ARP

NAT

ModSec

DNS/Passiv DNS/Reverse DNS lookup

Proxy/reverse Proxy

Load balancing

TCP dump

PCAP
packet capture...

ipsec videor

#Opgaver
#Uge
###Opgave 4

Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.

computer/netværkskort lag 2
router lag 3

Udvælg 2-3 forskellige enheder fra listen

Find specifikationerne for enheden

fandte model number bag routeren og googlede den
router
https://www.normann-engineering.com/products/product_pdf/cable_modems/technicolor/EN_CGA4233.pdf

googlede hvordan man finder nic på linux og fandte nedenstående link
https://www.cyberciti.biz/faq/linux-list-network-cards-command/

kørede lspci | egrep -i --color 'network|ethernet|wireless|wi-fi'

men fik en fejl der sagde "egrep is obsolecent using grep -E"
googlede
computer/netværkskort
Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 11)
bedste jeg kune finde https://www.os2world.com/wiki/index.php/Realtek_RTL8111/8168/8411_PCI_Express_Gigabit_Ethernet_Controller_Hardware_Report

Network controller: Intel Corporation Wireless 7265 (rev 59)
https://www.intel.com/content/www/us/en/products/sku/83635/intel-dual-band-wirelessac-7265/specifications.html


Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?

###Opgave 5

skrive om bottleneck

###Opgave 6 

prøvede at installere iperf med sudo pacman -S iperf men jeg så at der stod iperf3 i kommandoen i opgaven så jeg lavede en query med
sudo pacman -Ss iperf og så der var en iperf3. jeg så der var en tab der hed iperf3 servers på iperfs hjemmeside så jeg gik derind og
kørt kommandoen der stod i opgaven og brugte den øverste server.

###Opgave 6b

Jeg glemte kommandoen for at vis ip address på linux så jeg startede med at google det. Kørte ip a og fandte min ip address.
Jeg googlede også "What is my ip address" men fik ikke den samme ip address. Addressen der blev vist  i terminalen så mere rigtig
ud så fordi den startede med 192.168 så jeg brugte den men noterede stadig den anden. Jeg installerede iperf3 på wsl på min bærbar og
brugte den samme kommando men erstatede ip addressen. Kommandoen virkerede ikke som forventede og jeg var meget sikker at problemet var porten.
Jeg googlede problemet og fandteud af hvordan man åbner en port på manjaro (https://wiki.manjaro.org/index.php?title=Firewalls). Jeg kørte
sudo ufw 5201/tcp fordi 5201 er den default port når man kører iperf3 -s. Jeg kørte iperf3 serveren og prøvede igen med den med port 5201
og den virkede.

# Uge 8

netværksdiagram
![netværksdiagram](images/netdiagram.png)  

subnet og router  
https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h

Virtuelle netværk i VMware  
https://www.sysnettechsolutions.com/en/vmware-workstation-custom-specific-virtual-network/

# Uge 9

## Opgave 7
Lav en IP subnet oversigt over dine devices og de tilhørende interne og eksterne netværk.
Anvend meget gerne det netværk i påtænker til semester projektet!

Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.
https://docs.netbox.dev/en/stable/

Sæt netbox VM op og dokumentér dit netværk via netbox interfacet. Brug getting started fra netbox dokumentationen (som er lavet med mkdocs!!)
Hvis i allerede har netværket til semester projektet skitseret eller tegnet som netværksdiagram så anvend det
Alternativt kig på netværket i den virksomhed du er ansat i
Ellers søg på nettet efter large network diagram eller large network topology

Hvis i bruger en virksomhed i opgaven så se på det IPAM dokumentation som virksomheden allerede har og forhold dig til det.
Alternativt research hvilke andre måder i kan lave IPAM dokumentation på. Noter det som en liste i jeres dokumentation.

## NTOP/netflow opgaven  

nprobe  

Googlede hvordan man installerede ntopng på debian på fandte den nedenstående link  
https://www.howtoforge.com/how-to-install-ntopng-network-monitor-on-debian-10/  

NTOPNG er ikke i debians repository så første skulle jeg tilføj den  
`wget http://apt.ntop.org/buster/all/apt-ntop.deb  
dpkg -i apt-ntop.deb`  

Opdatere repositoryen med `sudo apt update` og bagefter kan jeg installere NTOPNG  
`apt-get install pfring-dkms nprobe ntopng n2disk cento -y`  

Start NTOPNG service med  
`sudo systemctl start ntopng  
sudo systemctl enable ntopng`  

Se startussen af NTOPNG med `sudo systemctl status ntopng`  
Jeg kan se at der stå active.  

Bagefter skal jeg ændre konfigurationsfiler. Det kan jeg gøre med `sudo vim /etc/ntopng/ntopng.conf`  
Jeg ændrer de nedenstående linjer  
`##Define the network interface for network monitoring.  
-iens33  
##Define the HTTP port for web server.  
-w=3000`  
iens33 er navnet af netværksinterfacet som jeg fandte ved at bruge `ìp a`. 3000 er portnummeret.  

Bagefter tilføjer jeg  
`--local-networks "192.168.146.135/24"  ## give your local IP Ranges here.  
--interface 1`  

Genstart NTOPNG med `sudo systemctl restart ntopng`  
Tilgå NTOPNG ved at skrive 192.168.146.135:3000 i browseren  

# Uge 10

## Opgave 9
Information

Protokoller er udbredte indenfor netværk, nogle af dem implementerer sikkerhed, nogle af dem gør ikke.
Formålet med denne opgave er at træne brugen af wireshark til at genkende protokoller.
Instruktioner

1. Find to-tre protokoller fra wiresharks test pakker som du kan genkende
2. Forstå hvad der foregår, evt. vha. google
3. Tag noter, og vis det næste gang

## Opgave 10

Information

Netværkstrafik er en vigtig del af sikkerheds arbejdet, det viser ret detaljeret hvad der foregår på netværket og kan bruges til at identificere uønsket trafik.

Wireshark kan også bruges til at undersøge hvad der historisk er foregået på netværket, dette anvedes blandt andet til at forstå hvordan trusselsaktører har udfært, eller forsøgt at udføre et angreb.

Netværkstrafik kan gemmes som PCAP som wireshark kan skrive/læse.

Husk at du kan bruge wireshark til at følge f.eks en TCP stream.
Når du kigger på netværkstrafik er det også nyttigt at bruge filtre i wireshark.
Instruktioner

1. Start wireshark på lokal net/firma net og saml noget data op
2. Gem PCAPS
3. Se på det: er der ukendte protokoller? ukendte ip adresser? andet interessant?
4. Dyk ned i 2-3 streams, og forstå hvad der foregår.
5. Tag noter og vis det næste gang

## SSL dump opgave

![SSLDump](images/SSLDump.png)  

## Uge 11

Forbinde vm til vmnet11  
startede vm  
læst information på http://192.168.111.20/  
åbnet http://192.168.111.20:9090/  
![PromethuesErrorTime](images/PromethuesErrorTime.png)  
![GrafanaNoData](images/GrafanaNoData.png)  

Jeg gik ind i browse og så "prometheus test" jeg kiggede på den og brugte den som en guide.  

Prometheus  
http://192.168.111.20:3000/d/R8vpyRf4z/test?orgId=1  
![BootTimeGrafana](images/BootTimeGrafana.png)  

Loki  
http://192.168.111.20:3000/d/87C48Rf4z/authlog?orgId=1  
![GrafanaLogBrowser](images/GrafanaLogBrowser.png)  
![GrafanaAuthLog](images/GrafanaAuthLog.png)  

# Uge 12

## Opgave 11 - DNS basics

Domain Name System - DNS bruger du hver dag, det samme gør sikkerhedsprofesionelle og trusselsaktører.  
Formålet med denne opgave er at få indsigt i hvordan DNS grundlæggende fungerer.  
Instruktioner  

1. Gennemfør rummet https://tryhackme.com/room/dnsindetail  
2. Skriv dine opdagelser ned undervejs og dokumenter det på gitlab  

Besvar herefter følgende om DNS og dokumenter undervejs:  

1. Forklar med dine egne ord hvad DNS er og hvad det bruges til.  
&nbsp;&nbsp;&nbsp;&nbsp;DNS oversætter human-readable domain name til en ip-addrress som computeren kan bruge til at finde serveren.  

2. Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? https://blog.cloudflare.com/dns-encryption-explained/  
&nbsp;&nbsp;&nbsp;&nbsp;Alle dele af CIA er aktuelle. Kryptering af DNS queries og answers med DNS over TLS (DoT) eller DNS over HTTPS (DoH).  

3. Hvilken port benytter DNS?  
&nbsp;&nbsp;&nbsp;&nbsp;53  

4. Beskriv DNS domæne hierakiet  
![DNS-hierarchical-tree](images/DNS-hierarchical-tree.jpg)  
fra https://cloudinfrastructureservices.co.uk/what-is-dns-hierarchy/  

5. Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.
&nbsp;&nbsp;&nbsp;&nbsp;A: IPv4  
&nbsp;&nbsp;&nbsp;&nbsp;AAAA: IPv6  
&nbsp;&nbsp;&nbsp;&nbsp;MX: email server  
&nbsp;&nbsp;&nbsp;&nbsp;TXT: Text  
&nbsp;&nbsp;&nbsp;&nbsp;CNAME: subdomain  

6. Brug nslookup til at undersøge hvor mange mailservere ucl.dk har
![nslookupUCLmail](images/nslookupUCLmail.png)  

7. Brug dig til at finde txt records for ucl.dk
![digUCL](images/digUCL.png)  

8. Brug wireshark til:  
&nbsp;&nbsp;&nbsp;&nbsp;a. Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dns  

&nbsp;&nbsp;&nbsp;&nbsp;b. Lav screenshots af eksempler på DNS trafik
![wiresharkDNS](images/wiresharkDNS.png)  

## Opgave 12 - Rekognosering  

Information  

Rekognosering bruges af både trusselsaktører og sikkerhedsprofessionelle.  
Rekognosering giver overblik over hvilke tekniske foranstaltninger en virksomhed har.  
Den information kan så bruges eller misbruges.  

Formålet med opgaven er at du lærer forskellige værktøjer til rekognosering og især værktøjer der bruger DNS opslag.  

Værktøjer du skal undersøge er:  

- whois  
- nslookup  
- dig  
- DNSdumpster  
- shodan  
- Securitytrails  

Instruktioner  

1. Gennemfør THM rummet: [https://tryhackme.com/room/passiverecon](https://tryhackme.com/room/passiverecon)  
2. Læs om og registrer en konto på securitytrails.com  
3. Beskriv forskellen på aktiv og passiv rekognoscering  
4. Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål  
5. Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål  
6. Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til  
7. Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed  
8. Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!  

# Uge13

## Opgave 14 - WireShark basics  

Information  

Vi vil connecte til en HTTP server og se på trafikken  

Formålet med opgaven er at forstå netværkstrafik og se på HTTP  
Instruktioner  

1. Start wireshark  
2. Gå ind på http http://mirror.one.com/debian/ og brug din browsers view source  
![DebianMirrorPageSource](images/DebianMirrorPageSource.png)   
3. Genfind trafikken i wireshark, brug follow tcp stream  
![DebianMirrorTCPStream](images/DebianMirrorTCPStream.png)   
4. Hent den samme side via https https://mirror.one.com/debian/  
5. Genfind trafikken i wireshark, brug follow tcp stream  
![DebianMirrorTCPStreamHTTPS](images/DebianMirrorTCPStreamHTTPS.png)   
6. Brug curl/wget fra kommando linien til at hente HTTP fra linket i punkt 3  
![CURLMirrorDebian](images/CURLMirrorDebian.png)   
7. Sammenlign med resultatet fra browserens view source  

## Opgave 15 - Wireshark OSI  

Information  

Vi vil lave DNS opslag, genfinde dem i wireshark og se på hvad wireshark fortæller om OSI lagene.  

Formålet med opgaven er at konsolidere viden om OSI modellen og få lidt mere praktisk erfaring med wireshark.  
Instruktioner  

1. Start din Kali maskine og check at den har internet forbindelse  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg kan tilgå hjemmesider så der er internetforbindelse.
2. Start wireshark og lyt på det udvendige interface  
3. Start en kommando prompt og brug dig til at lave et opslag på f.eks. dr.dk  
4. Gennemgå trafiken i wireshark og fortæl hvad der ses  
![WiresharkDigDR](images/WiresharkDigDR.png)   
5. Tegn et netværks diagram med de vigtigste enheder  
![NetDiagramDigDR](images/NetDiagramDigDR.png)   
6. Ud fra wireshark, find ip adresser og MAC adresser på disse enheder  

## Opgave 16 - wireshark NAT  

Information  

Denne opgave minder om opgave 15 Formålet med opgaven er at kunne arbejde med wireshark.  
Instruktioner  

1. Start din Kali maskine og en router, check at der er internet forbindelse  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg havde en router fra https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h.  
&nbsp;&nbsp;&nbsp;&nbsp;Den er forbundet til vmnet8, 11, og 12. Kali maskinen er forbunet til vmnet11. Jeg pingede 8.8.8.8 på kali.  
2. Tegn et netværksdiagram med de relevante enheder  
![PingTraceroute](images/PingTraceroute.png)   
3. Find to egnede steder i netværket hvor man kan lytte til host trafik og til ekstern internet trafikken  
VMNET8 og wifi
4. Start to instances af wireshark eller tcpdump der lytter på de valgte punkter  
5. Connect til en hjemmeside fra kalien. Brug curl eller wget for at reducere mængden af trafik  
6. Genfind trafikken i begge wireshark streams  
7. Vis forskellene på lag 2 og 3  

## Opgave 17 - logs

Formålet med opgaven er at få et indtryk af log og hvad man kan læse af dem.  

Specifikt vil vi connecte via ssh og genfinde dataene i loggen.  
Instruktioner  

1. Start default open bsd router og kali linux  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg havde en router fra https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h.  
&nbsp;&nbsp;&nbsp;&nbsp;Den er forbundet til vmnet8, 11, og 12. Kali maskinen er forbunet til vmnet11. Jeg pingede 8.8.8.8 på kali.  
2. Log ind i konsollen på routeren og notér ip adressen på det eksterne interface: ifconfig em0  
&nbsp;&nbsp;&nbsp;&nbsp;Kiggede rundt på Mortens gitlab og fandte login her https://moozer.gitlab.io/course-networking-basisc/Bonus/ob_ip_update/  
&nbsp;&nbsp;&nbsp;&nbsp;root:root123  
![BSDifconfig](images/BSDifconfig.png)   
3. Åben en terminal i kali, og forbind routern: ssh sysuser@<router ip>  
4. Se hvad der er af logs: cd /var/log efterfult af ls  
5. Hvad er der af log filer?  
&nbsp;&nbsp;&nbsp;&nbsp;- messages er vigtig i bsd (på linux er den tilsvarende syslog)  
&nbsp;&nbsp;&nbsp;&nbsp;- auth indeholder authentication events  
&nbsp;&nbsp;&nbsp;&nbsp;- Der er ikke DNS logs tilgængelig i default konfigurationen, men det kan slås til. Se logfile directive  
6. Tricks: tail -f authlog og start en ny ssh connection til routeren. Hvad sker der?  
7. Log filer bliver allerede hentet ind fra grafana serveren til grafana serveren. Start server-mon02.  
8. Gå til grafana web interface. Default url er http://192.168.111.20:3000  
9. Gå til explore og lav en simpel loki søgning, f.eks. {filename="/var/log/syslog"} |= "ssh"  
10. Lav også: {filename="/var/log/auth.log"} |= "ssh"  
11. Sammelign  

# Uge 11

# Opgave 18 - ARP basics  
Information  

Ønsker man at lave man-in-the-middle angreb på lag 2, så er ARP protokollen relevant.  

I denne op gave ser vi på hvordan den virker.  
Instruktioner  

1. Start Kali og router på et internt netværk  
2. Start wireshark i kali  
3. Find ARP requests Forklar hvad der ses  
![ARP](images/ARP.png)   
kalimaskinen spørger hvem har 111.2 via broadcast og får en mac addresse tilbage  
4. Ping en ikke-eksisterende ip adresse og find arp requests Forklar hvad der ses  
![1234ARP](images/1234ARP.png)   
5. Kør arp -e Forklar hvad der ses  
![ARPe](images/ARPe.png)  

# Uge 17  

# Opgave 26  

Information

Intrusion detection er detektering af uønsket netværkstrafik og intrusion prevention er forhindring af uønsket netværkstrafik.
Der findes overordnet 2 typer af detektering, signatur baseret eller anomali baseret.

I denne opgave skal i undersøge hvad intrusion detection og prevention er og hvad forskellen er på signatur baseret og anomali baseret.

Opgaven skal løses i jeres teams og dokumenteres på gitlab.  
Instruktioner  

Brug nedenstående links til at besvare følgende spørgsmål:  

1. Forklar med egne ord hvad et intrusion detection system gør, underbyg med troværdige kilder  

IDS skanner trifikken og kan dedektere noget der kan være skadelig. IDS kan side inline men behøver ikke  
Ross Andersen, Security engineering kapitel 21.4.3 Intrusion Detection  
https://www.paloaltonetworks.com/cyberpedia/what-is-an-intrusion-prevention-system-ips  

2. Forklar med egne ord hvad et intrusion prevention system gør, underbyg med troværdige kilder

IPS og kan dedektere og kan gøre noget mod noget der kan være skadelig. IPS skal sidde inline  
https://www.paloaltonetworks.com/cyberpedia/what-is-an-intrusion-prevention-system-ips  

3. Forklar forskellen på et Netværks IDS/IPS (NIDS) og et host IDS/IPS (HIDS)  



4. Forklar med egne ord hvad signatur baseret detektering er, underbyg med troværdige kilder  
5. Forklar med egne ord hvad anomali baseret detektering er, underbyg med troværdige kilder  
6. Forklar mulige problemer med hhv. signatur og anomali baseret detektering  

I må gerne medtage anden viden i opnår igennem opgaven, for eksempel ved at søge på nettet efter ting i ikke umiddelbart forstår eller bare gerne vil vide mere om.
Der er et par links herunder der kan hjælpe med at finde svar på ovenstående spørgsmål.  

# Opgave 27 - Suricata opsætning

Information

I denne opgave skal du installere, konfigurere og teste suricata på en virtuel maskine.  
Du skal starte med at bruge suricata på en meget simpel måde, i offline mode, med pcap filer og vores egne regler.  
Formålet er at lære hvordan suricata installeres, hvor konfigurationsfiler befinder sig etc. inden du loader mange tusind regler og kører i live mode.

Mens du arbejder skal du lave dokumentation af hvad du gør og også indsætte det output du får når du kører de forskellige kommandoer.

Instruktioner

1. Hent og åbn en [basis debian vm](https://drive.google.com/file/d/17rhq9i1fUdBzMo-S53T_Km0YRlcT1S97/view?usp=sharing) uden desktop installeret. 
    - Der er 2 brugere. *user:user1234* og *root:root1234*
    - ssh server er installeret så du kan tilgå maskinen fra en anden VM, f.eks din Kali
    - Network adapter skal ændres til det vmnet du ønsker at anvende, du skal have internetforbindelse (ie. vmnet8)
2. Tegn et netværks diagram over netværket
3. Installer Suricata
    - `sudo apt-get install suricata` [kilde](https://suricata.readthedocs.io/en/latest/install.html#debian)
    - kontroller at suricata er installeret: `suricata -V`
4. Konfigurer Suricata med din foretrukne tekst editor. Konfigurations filen er i `/etc/suricata/suricata.yaml`
    1. Under `default-rule-path` kan du angive en mappe hvor suricata henter regler, du kan angive flere lokationer. Ret så det ligner nedenstående: 
      ```bash 
        default-rule-path: /etc/suricata/rules

        rule-files:
          - local.rules
      ```
```sudo apt-get install vim```  
```sudo vim /etc/suricata/suricata.yaml```  
```grep default-rule-path /etc/suricata/suricata.yaml```  
![Opgave_27_vim](images/Opgave_27_vim.png)   

5. Opret filen `local.rules` i mappen `/etc/suricata/rules` og tilføj denne linie (regel) i `local.rules` filen:  
    `alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)` 

6. Test at suricata virker og accepterer din nye regel (det vil sige ingen errors i output), brug kommandoen `sudo suricata -T`, output bør ligne:
    ```bash
      user@debian-base:~/pcap-files$ sudo suricata -T
      22/4/2023 -- 14:04:27 - <Info> - Running suricata under test mode
      22/4/2023 -- 14:04:27 - <Notice> - This is Suricata version 6.0.1 RELEASE running in SYSTEM mode
      22/4/2023 -- 14:04:27 - <Notice> - Configuration provided was successfully loaded. Exiting.
    ```  
    Hvis der er fejl så fejlfind og ret indtil der ikke er fejl.
![Opgave_27_Suricata-T](images/Opgave_27_Suricata-T.png)   

7. Lav en mappe der hedder `pcap-files` i user home og gem filen [ping.pcapng](../pcap/ping.pcapng) i mappen  
```mkdir pcap-files```  
```cd pcap-files```  
```wget https://ucl-pba-its.gitlab.io/23f-its-nwsec/pcap/ping.pcapng```  
![Opgave_27_Suricata_PingPcap](images/Opgave_27_Suricata_PingPcap.png)   

8. Kør `pcapng` i suricata med kommandoen `sudo suricata -v -r ping.pcapng` og kig på output, hvad ser du?  
9. Undersøg indholdet i de 4 nye filer som suricata har genereret i pcap-files mappen, hvad er indholdet og hvad kan det bruges til?  
Forstå og dokumenter det.
    ```bash
      user@debian-base:~/pcap-files$ ls -l
      total 24
      -rw-r--r-- 1 user user 6571 Apr 22 14:09 eve.json
      -rw-r--r-- 1 user user  308 Apr 22 14:09 fast.log
      -rw-r--r-- 1 user user 1152 Apr 22 13:07 ping.pcapng
      -rw-r--r-- 1 user user 1899 Apr 22 14:09 stats.log
      -rw-r--r-- 1 user user 1735 Apr 22 14:09 suricata.log

suricata.log  
information om serveren og mængde af alerts og regler  
stats.log  
Information om pakkerne som bytes
fast.log  
Prioritet, protocol og ip  
eve.json  
Information i json format  

# Opgave 28 - IDS/IPS Regler  

Information  

Suricata bruger regler til at detektere signaturer i netværkstrafik. 

Regler er opbygget af 3 dele: 

- **action** definerer hvad der skal ske når der er et match, f.eks alert eller drop
- **header** definerer retning, ip adresser/netværk, protokol og port
- **rule options** definerer yderligere konfiguration af en regel

Du har allerede lavet din første regel i opgave 27 - Suricata opsætning hvor du tilføjede denne regel til local.rules filen:  
`alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)`


Her er en forklaring af hvordan reglen er opbygget:

- `alert` er **action**
- `icmp any any -> any any` er **header** `icmp` er protokollen, `any any -> any any` betyder fra alle ip adresser på alle porte til alle ip adresser på alle porte. Her kunne være specificeret en specifik ip adresse/netværk og port i både afsender og modtager. Retningen `->` kunne have været specificeret i begge retninger `<>`.
- `(msg:"ping/icmp detected"; sid:5000000;)` er **rule options**, `msg` er den tekst der skrives i fast.log og eve.json. `sid` **skal** angives **og** være unik for hver regel.

Det kan være en fordel at have pcap filen åben i wireshark samtidig med at du laver opgaven.  

Instruktioner

1. Læs om regler i [Suricata dokumentationen](https://suricata.readthedocs.io/en/suricata-6.0.11/rules) og dan dig et overblik over mulighederne.  
2. Lav en ny mappe på den vm hvor du har installeret Suricata
```mkdir suricata```  
3. På suricata maskinen hent pcap filen fra [https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip](https://www.malware-traffic-analysis.net/2018/07/15/index.html) (brug wget, **zip password er _infected_**)
```wget https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip```
4. Skriv en regel i `/etc/suricata/rules/local.rules` der laver en alert på udp trafik fra `10.0.0.201 port 63448` til `5.138.53.160 port 41230`  
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?  
![Opgave_28_Rules](images/Opgave_28_Rules_HTTP.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_RulesLoaded_HTTP.png)   
![Opgave_28_HTTP_Rejected](images/Opgave_28_HTTP_Rejected.png)   

5. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor http content type er **image/jpeg**  
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?  
![Opgave_28_RulesLoaded](images/Opgave_28_Rules_JPEG.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_RulesLoaded_JPEG.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_JPEG_Rejected.png)   

6. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor http content type er **application/x-bittorrent**  
![Opgave_28_RulesLoaded](images/Opgave_28_Rules_Torrent.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_RulesLoaded_Torrent.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_Torrent_Rejected.png)   

7. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor ordet **Betty_Boop** indgår.  
Dokumenter hvor mange alerts du ser, samt hvilke ip adresser og porte der er involveret i kommunikationen ?  
![Opgave_28_RulesLoaded](images/Opgave_28_Rules_BetttyBoop.png)   
![Opgave_28_RulesLoaded](images/Opgave_28_BetttyBoop_Rejected.png)   

# Opgave 29 - Suricata live

Information

Indtil nu har du brugt Suricata til at analysere pcap filer, ikke fordi det er det bedste værktøj til det, men for at få en forståelse af hvad suricata er og hvordan detekterings regler fungerer. 

I praksis vil du nok bruge regler som er skrevet af andre, måske suppleret med nogle få af dine egne regler.

Der findes åbne community regler som du kan bruge i et IDS/IPS som Suricata eller Snort.  

Emerging Threats er et godt sted at starte når det gælder community regler, de har tusindvis af regler og det kan godt være overvældende at arbejde med dem fra terminalen.  

Derfor skal du i denne øvelse bruge en [opnsense](https://opnsense.org/) router som giver mulighed for at installere suricata som en pakke i routeren.  

Routeren har et web interface der giver et rimeligt overblik over hvad der foregår.

Du skal selvfølgelig dokumentere dit arbejde undervejs.

Instruktioner

1. Hent opnsense [.ova](https://drive.google.com/file/d/1BIhjv21642vOsmE6RNqa-jX9MtzrySCx/view?usp=sharing) filen
2. Mens du venter, så dan dig et overblik over hvordan [Emerging Threaths](https://rules.emergingthreats.net/open/) reglerne er organiseret og tag også et kig på et par regler, de kan hurtigt blive komplekse!
3. Åbn og konfigurer opnsense maskinen: 
    1. `vmnet8` på `network adapter 1`
    2. `vmnet1` på `network adapter 2`
    3. Start maskinen  
    4. Noter routerens lan og wan adresser.  

    Info om opnsense vm opsætning:
    ```bash
      root:root1234
      wan nwadapter 1: addr from dhcp
      lan nwadapter 2: 192.168.1.0/24 dhcp server enabled
      mngt iface: 192.168.1.1
    ```

![OpenSenseLANWAN](images/OpenSenseLANWAN.png)   

4. Sæt en vm (victim) på opnsense lan interfacet `vmnet1`
5. Åbn en browser på victim og log på opnsense management interfacet: `http://192.168.1.1`
5. Sæt en kali maskine (attacker) på opnsense lan interfacet `vmnet1`
6. Tegn et netværksdiagram over dit netværk
7. I opnsense management interfacet skal du navigere til `services->intrusion detection->administration`  
Her skal du vinge af i `enabled`, `Promiscuous mode` og du skal sætte interfaces til både `wan` og `lan`.  
Husk at klikke på `Apply` i bunden af vinduet!  
![IDSsettings](images/IDSsettings.png)  
8. Gå til `Download` fanen og find regelsættet der hedder `ET open/emerging-scan` og ving det af, klik på `enable selected` og derefter `Download & update rules`  
9. Gå til `Rules` fanen og marker alle regler (der er 361! så find knappen der vælger dem alle på en gang) og aktiver dem, klik derefter på `Apply`  
10. Gå til `Alerts` fanen som gerne skulle være tom, det er her du vil kunne se hvad der registreres af suricata
11. På victim maskinen start en service der lytter på en port, f.eks med `sudo python3 -m http.server`
11. På Kali maskinen lav en nmap scanning på victim maskinens ip adresse f.eks `sudo nmap -v -sV 192.168.1.101` (erstat ip med den adresse som din victim maskine har)
![NMAPsuricata](images/NMAPsuricata.png)   
12. Refresh `alerts` og du burde kunne se at der er blevet detekteret noget  
![SuricataAlerts](images/SuricataAlerts.png)  
Hvis der ikke er nogle alerts så gennemgå ovenstående og fejlfind :-)
11. Undersøg dine alerts og genfind reglerne på [Emerging Threaths](https://rules.emergingthreats.net/open/)
![NmapAlert](images/NmapAlert.png)  
12. Lav en nmap scanning der ikke bliver fanget af suricata, altså ikke alerter.  
Forklar hvilken type nmap scanning du lavede 
![NmapSynScanSuricata](images/NmapSynScanSuricata.png)  
13. Lav en nmap scanning der laver så mange alerts som muligt (mere end 4)  
Forklar hvilken type nmap scanning du lavede 

**Bonus**

Nu hvor du har et lille ids/ips lab så har du nu mulighed for at lege og lære :-)  
Er du f.eks ikke nysgerrig efter at prøve flere regler ? måske aktivere alle reglerne og afprøve med forskellige Kali værktøjer!

1. Aktiver alle reglerne i opnsense suricata
2. Lav et angreb med [metasploit framework](https://www.kali.org/tools/metasploit-framework/) fra kali maskinen
3. Der er en [youtube video](https://youtu.be/oUp09mcn3Zc) der viser hvordan

Find selv på andre værktøjer du vil prøve og tænk over om du kan bruge det du har lært om intrusion detection og intrusion prevention, i semesterprojektet.

# Uge 18

## Opgave 30 - Certifikater

Certifikater er et af fundamenterne i det moderne web.

1. Gå til [https://dr.dk](https://dr.dk)
2. Klik på hængelåsen ved siden af url'en, og vis certifikat information
3. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?

  [Certificate chains](https://knowledge.digicert.com/solution/SO16297.html) er relevant her  

  ![DRCertificate.png](images/DRCertificate.png)  

4. Gå til [https://ucl.dk](https://ucl.dk)
5. Klik på hængelåsen ved siden af url'en, og vis certifikat information
6. Hvem har udstedt certifikatet, hvem er det udstedt til og hvad er levetiden?
![UCLCertificate](images/UCLCertificate.png)  
7. Start en kali
8. Åben en terminal og kør `openssl s_client ucl.dk:443`
    
    Brug `ctrl+c` til at stoppe kommandoen

9. Sammenlign informationen fra browseren

## Opgave 31 - Let's encrypt

I denne opgave skal vil vi sætte en webserver op med et valid certifikat fra let's encrypt. Vi bruger digital ocean til det.

I opgavebeskrivelsen bliver der brugt Ubuntu 20.04. Man kan vælge at bruge 22.04 i stedet. Desuden bliver der brugt `sudo`. Dette er ikke nødvendigt hvis man er logget ind som root.

Til denne opgave skal der bruges et digital ocean login og et domænenavn.


0. Lav en [droplet](https://docs.digitalocean.com/products/droplets/how-to/create/#create-a-droplet-in-the-control-panel)

  Dette er en cloud løsning, så man skal huske at slukke for sin virtuelle maskine npr man er færdig. Prisen er pt. $4/måned.

  Hvis man ikke kan lide at logge ind vha. ssh, så er der mulighed for at bruge "console" fra digital oceans web interface

1. Installer en webserver. Følg [denne](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04) guide.

  Det er ikke nødvendigt med "Server block" delen

```sudo apt update```  
```sudo apt install nginx```  

![DropletAllowList](images/DropletAllowList.png)  
![AllowNginxHTTP](images/AllowNginxHTTP.png)  
![DropletUFWStatus](images/DropletUFWStatus.png)  
![CTLNginxStatus](images/CTLNginxStatus.png)  
![NginxCurl](images/NginxCurl.png)  
![NginxStop](images/NginxStop.png)  
![NginxStart](images/NginxStart.png)  


2. Configurer et certifikat vha. let's encrypt. Følg [denne](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04) guide.

```sudo apt install certbot python3-certbot-nginx```  
```sudo nano /etc/nginx/sites-available/example.com```  
```...  
server_name example.com www.example.com;  
...```  

![NginxSyntaks](images/NginxSyntaks.png)  
![NginxFull](images/NginxFull.png)  
![UFWStatusNginxFull](images/UFWStatusNginxFull.png)  
![CertbotFail](images/CertbotFail.png)  

3. Check at det virker ved at gå ind på hjemmesiden, og se på certifikatet

## Opgave 32 easy rsa

Let's encrypt givere mulighed for at udstede certifikater som bruges eksternt. De fleste organisationer bruger en internt PKI struktur, e.g. fra deres windows domæne controller.

Der er også mulighed for DNS-01 ACME metoden til at udstede certifikater com bruges internt, men det er out-of-scope her.

Man kan sætte en intern PKI op vha. easy-rsa. Dette bruges f.eks. til certikater til VPN forbindelser eller andre cruver certifikater.


0. Kend til certifikater, certificate signing request, certificate authority og certificate chains.

  Se e.g. [her](https://dahamposithapathiraja.medium.com/how-https-works-5c44fd3f694c) og [her](https://knowledge.digicert.com/solution/SO16297.html)

1. Installér en easy-rsa pki

  Se [dokumentationen](https://easy-rsa.readthedocs.io/en/latest/#obtaining-and-using-easy-rsa)

  Dette burde ske på en "sikker" host som ikke er webserveren, men til øvelsen kan det være den samme.

2. Configurér en CA

  Se [dokumentationen](https://easy-rsa.readthedocs.io/en/latest/#using-easy-rsa-as-a-ca). Det samme bare fra en [anden kilde](https://wiki.gentoo.org/wiki/Create_a_Public_Key_Infrastructure_Using_the_easy-rsa_Scripts)

2. Opret en ny vm med en webserver

  Brug evt. webserveren fra digital ocean, eller følg [guiden](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)

3. Opret et certifikat til webserveren

4. Tilføj certifikatet til webserveren

  Det er "bare" at kopiere en fil og pege på den fra konfigurationsfilen. Se f.eks. [her](https://phoenixnap.com/kb/install-ssl-certificate-nginx)

5. check at det virker vha. browser og/eller curl

# Uge 19 - Defending networks

## Opgave 34 - MITRE att&ck framework

De har mange resource tilgængelige.

* [Getting started](https://attack.mitre.org/resources/getting-started/)
* [Deres ebook](https://www.mitre.org/sites/default/files/2021-11/getting-started-with-attack-october-2019.pdf)

## Instruktioner

1. Lav en hurtig beskrivelse af hvad MITRE att&ck framework er. Inkludér de tre hovedkategorier: Tactics, Techniques and Procedures
    MITRE att&ck framwork er en samling af teknikker brugte i it-sikkerhed. Når en organisation bliver angrebet kan de følge MITRE for at mitigere skaden.  i
    Tactics er angreberens mål.
    Techniques er hvordan de opfylder deres mål for eksempel sudo og sudo caching.
    Procedures er en detaljeret beskrivelse af angrebet.

2. Bonus: Hvorfor TTPs? Hvor kommer det fra?

3. Find 2-3 eksempler i hver kategori. Supplér med en sætning eller to om hvad de dækker over.
    * Tactics
        * Initial access  
        * Privelidge escaltion
        * Defense evasion  
    * Techniques
        * Drive-by comprimise
        * Trusted relationship
        * Valid accounts  
    * Procedures
        * Proton
        * Dok
        * Cobalt strike

## Opgave 35 - Netværkssegmentering

## Information

Segmentering af netværket

En MITRE att&ck mitigation er [network segmentation](https://attack.mitre.org/mitigations/M1030/)

## Instruktioner

1. Hvad er netværkssegmentering? Hvad bruges det til?

2. Konstruér et simple netværk med DMZ zone og brugere. 

3. Gennemgå netværket med en CIA indfaldsvinkel, og beskriv problemerne.

4. Beskriv hvilke problemer i netværket som netværkssegmentering reducerer.

## Opgave 36 - ARP

## Instruktioner

    1. Læs om [ARP spoofing](https://attack.mitre.org/techniques/T1557/002/) og [DHCP spoofing](https://attack.mitre.org/techniques/T1557/003/) som techniques

2. Se på mitigations og giv eksempler på hvordan de foreslåede tiltag kan begrænse spoofing.
    * [User training](https://attack.mitre.org/mitigations/M1017/)
    * [Filter network traffic](https://attack.mitre.org/mitigations/M1037/)

3. Se på detections og giv eksempler på hvordan de foreslåede tiltag kan detektere spoofing.
    * [Network traffic content](https://attack.mitre.org/datasources/DS0029/#Network%20Traffic%20Content)
    * [Network traffic flow](https://attack.mitre.org/datasources/DS0029/#Network%20Traffic%20Flow)

## Opgave 37 - DNS

## Information

Vi har tidligere snakket om problemer med ARP og spoofing. Se f.eks. opgaverne med [ettercap](19_ettercap.md)

Det er også med i MITRE att&ck framework.

## Instruktioner

1. DNS er meget brugt i angreb
    * [DNS og C2](https://attack.mitre.org/techniques/T1071/004/)
    * [recon](https://attack.mitre.org/techniques/T1590/002/)
    * [attacker DNS server](https://attack.mitre.org/techniques/T1583/002/)
    * [compromised DNS](https://attack.mitre.org/techniques/T1584/002/)
    * [Passive DNS](https://attack.mitre.org/techniques/T1596/001/)
    * Der er flere. Søg selv efter DNS.

2. Er det noget der bliver brugt af angriberne?

3. Hvad er der er mitigeringer?
    * DNS og C2
        * [Filter network traffic](https://attack.mitre.org/mitigations/M1037)
        * [Network intrusion](https://attack.mitre.org/mitigations/M1031)
    * recon
        * [Pre-comprimise](https://attack.mitre.org/mitigations/M1056)

4. Hvad er der af detection?
    * DNS og C2
        * [Network traffic content](https://attack.mitre.org/datasources/DS0029/#Network%20Traffic%20Content)
        * [Network traffic flow](https://attack.mitre.org/datasources/DS0029/#Network%20Traffic%20Flow)
    * recon
        * recon er meget svært at detekere.
5. Hvordan kan suricata og ssl inspection bruges i denne kontekst?
Suricata og ssl inspektion kan hjælpe med at detektere angrebere. Det kan blive brugte med network traffic flow og network traffic control.

# Uge 20

## Opgave 38 - Angrebsflade (attack surface)

I denne opgave skal i undersøge hvad attack surface og attack vector er.  
I skal også undersøge hvordan det kan bruges af både aktører og trusselsaktører.

Der er givet nogle links til at komme i gang med at researche, find gerne selv flere links men husk at være kritiske ifht. kilden.
Det er vigtigt at i snakker om spørgsmålene så i opnår en fælles velreflekteret forståelse og viden.

## Instruktioner

Besvar følgende:

1. Forklar hvad attack surface er
2. Forklar hvad attack vector er
3. Hvad siger CIS18 ?
4. Hvad siger MITRE ? (husk sub techniques)

## Opgave 39 - Huller i suppen

## Information

Umiddelbart kan det virke mærkeligt at det, den dag i dag, stadig er muligt for trusselsaktører at få adgang til systemer men faktum er at det sker ofte.  
Hvad er årsagen til det? Og hvorfor "fikser" organisationer ikke bare deres systemer så de er "sikre" ?

I denne opgave skal i undersøge årsagerne til at systemer er usikre og sårbare. Links er til inspiration, find gerne flere selv hvis i synes det er nødvendigt. 

## Instruktioner

Besvar følgende:

1. Giv mindst 5 *typiske* eksempler på hvorfor systemer kan have sårbarheder, begrund dem sagligt med kildehenvisninger.
2. Skriv et bud på en procedure der kan bruges til at afdække attack surface.

## Opgave 40 - Aktive skanningsværktøjer

## Information

Netværk og services er som regel ikke en statisk størrelse og derfor er det en god ide jævnligt at afdække hvordan netværket ser ud.  
Der findes mange værktøjer til at afdække det, vi har tidligere kigget på passive værktøjer og skal nu kigge på et par aktive skanningsværktøjer.  

Husk at trusselsaktører bruger både passiv og aktiv skanning når de afdækker angrebsfladen og undersøger hvilke attack vectors de kan benytte.  
De leder efter ting der er eksponeret og giver hints til om komponenter og services er sårbare og kan udnyttes. 

Opgaven tager udgangspunkt i en virtuel maskine fra [vulnhub](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) som har konfigureret en del sårbare services.  
Det gør det muligt at bruge aktive *interne* skanningsværktøjer for at få indsigt i hvad de kan og hvilket output de giver.  
Husk at perspektivere til store netværk med mange maskiner og services!

Der findes også *eksterne* værktøjer som i skal afprøve, men ikke på den sårbare virtuelle maskine (der forhåbentlig ikke er eksponeret mod internettet...)

Formålet med opgaven er at få et indblik i hvad der kan opdages med aktiv skanning med henblik på at kunne detektere og mitigere eventuelle sårbarheder.

Opgaven skal laves i teams hvor i arbejder individuelt og i fællesskab snakker om det i finder ud af. I skal også rapportere i et fælles dokument.  

Her er et overblik over værktøjer til aktiv scanning/overvågning

**interne**  

- [NMAP](https://nmap.org/) [tutorial](https://phoenixnap.com/kb/nmap-commands)
- [sslscan](https://www.kali.org/tools/sslscan/)
- [openvas](https://www.openvas.org/)
- [nessus](https://www.tenable.com/products/nessus)


**eksterne**

- [shodan](https://www.shodan.io/) (søgninger, network monitoring)
- [whois](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage) Hvad viser den grønne knap med "find problems"
- [ssllabs](https://www.ssllabs.com/ssltest/) [test af ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest) 
- [security trails](https://www.recordedfuture.com/platform/attack-surface-intelligence) Attack Surface Intelligence


## Instruktioner

**Del 1: Interne værtøjer til aktiv skanning**

1. Hent [Vulnerable Pentesting Lab Environment: 1](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) ([mirror](https://drive.google.com/drive/folders/1gPSNA-2OF9dDJTdhUOcn2N3zPjRBUSWm?usp=sharing))
2. Lav et dokument til at rapportere det i finder undervejs.
3. Åbn maskinen i *vmware workstation* og konfigurer netværk. Maskinen henter sin IP fra DHCP så det vmnet du bruger skal være konfigureret med DHCP i *network manager*. Netværket skal ikke have internetadgang.
4. Sæt en kali maskine på samme vmnet.
5. Tegn et netværksdiagram med hvad du ved på nuværende tidspunkt.
6. Gør følgende med NMAP fra kali maskinen, du skal have wireshark kørende på samme tid:
    1. scan netværket med ping scan, gem scanning med `-oA <filename>`, hvad finder du og hvordan ser det ud i wireshark? 
    2. Gem en pcapng fil med trafikken fra wireshark
    3. Scan den sårbare maskine, du burde finde mindst 5 åbne porte.
    4. Hvilke services og versioner er på de 5 porte?
    5. Er der nogle services der er sårbare og hvor kan det undersøges? (hint! cve databaser og exploits) 
7. Gør følgende med sslscan fra kali maskinen:
    1. prøv ssl scan mod nogen af de services der kører på vple ie. `sslscan <vmip:port>` 
    2. Hvad er output fra sslscan? 
    3. er der services der bruger TLS ?
8. [gobuster](https://www.kali.org/tools/gobuster/#tool-documentation)/[dirbuster](https://www.kali.org/tools/dirbuster/)/[ffuf](https://www.kali.org/tools/ffuf/)/[wfuzz](https://www.kali.org/tools/wfuzz/) er værktøjer til at "fuzze" endpoints på services med henblik på at afdække hvilke endpoints der findes på en service.  
(måske skal du installere seclists [https://www.kali.org/tools/seclists/](https://www.kali.org/tools/seclists/))  
Afprøv de 4 værktøjer:  
    1. Brug wireshark når du fuzzer og gem relevant (http) trafik fra de forskellige værktøjer som en `.pcapng` fil
    2. Prøv gobuster med:  
    `gobuster dir --url http://<vmip>:1336 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee gobuster1336.txt`  
    og se hvad du finder ?
    3. prøv dirbuster med:  
    `dirb http://<vmip>:1336 /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee dirbuster1336.txt`  
    og se hvad du finder ?
    4. prøv ffuf med:  
    `ffuf -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -mc all -r  -fw 24 | tee ffuf1336.txt`  
    og se hvad du finder [ffuf cheatsheet](https://cheatsheet.haax.fr/web-pentest/tools/ffuf/)
    5. prøv wfuzz med:  
    `wfuzz -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -R 5 --hc 404 | tee wfuzz1336.txt`  
    og se hvad du finder
    6. Sammenlign resultaterne i de 4 filer du har lavet: 
        1. hvilket værktøj fandt mest ?
        2. hvilket værktøj tog længst tid ?
9. Hvilke services synes i er acceptable at have kørende og hvilke er der ikke nogen grund til at eksponere? Nogle specifikke porte?
10. Hvad vil i gøre for at "lukke" de uacceptable services?
11. Er aktiv skanning nok til at finde sårbarheder? Kan der gøres yderligere for at sikre netværk og services?

**Del 2: Eksterne værtøjer til aktiv skanning**

1. Shodan
    1. Lav en konto på shodan, den der er gratis (tip! hold øje på black friday hvis du vil have rabat til at blive member)
    2. Læs om hvad shodan kan ifht. netværks sikkerhed: [https://help.shodan.io/the-basics/what-is-shodan](https://help.shodan.io/the-basics/what-is-shodan)
    3. Læs om [shodan søgninger](https://help.shodan.io/the-basics/search-query-fundamentals)
    4. Lav følgende søgninger
        - `airplay port:5353`
        - `has_screenshot:true`
        - `http.title:"Nordex Control" "Windows 2000 5.0 x86" "Jetty/3.1 (JSP 1.1; Servlet 2.2; java 1.6.0_14)"`
    5. Udforsk selv yderligere muligheder, kig f.eks i de søgninger andre har lavet:  
    [https://www.shodan.io/explore/recent](https://www.shodan.io/explore/recent)
2. Whois
    1. Hvad viser den grønne knap med "find problems" på [whois](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage) ?
3. ssllabs
    1. Hvad kan i finde ud af på [ssllabs](https://www.ssllabs.com/ssltest/) 
    2. [test ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest) og se om der er problemer

## Opgave 41

# Emneliste

Nedenstående er listen over emner som kan trækkes på eksamensdagen.  
For alle emner er "i et sikkerhedsperspektiv" underforstået. 

1. **Krypteret trafik:** Formål, muligheder/begrænsninger, performance, aflytte plaintext/ciphertext/metadata  
    Hvordan kan man se at trafikken er krypterede?  
2. **Segmentering:** Routable og non-routable subnets, tunneller, firewalls, L2 security, performance/DOS, lateral movement
    Hvorfor segementere man et netværk?  
3. **Monitorering:** (funktionel): Beskriv de 3 typer (rød/grøn, grafer og logs) og hvad de bruges til både reaktivt og proaktivt
4. **DNS:** Resolve hostnavne, kryptering, certifikater, DNS og privacy
    Kan man kryptere DNS?  
    Hvad er fordele og ulemper med at kryptere/ikke krypetere DNS?
5. **Firewalls:** Funktion i netværket, statefull/stateless, perimeter sikkerhed vs. layered security
    Hvad er forskellen mellem hostbaseret og netværksbaseret firewall?  
6. **SSL inspection:** Implementation, certifikater, placering i netværket, problemer/muligheder for netværks ejeren og brugere
    Hvordan kan man spoofe en certifikat?  
    Hvordan kan man fange en spoofede certikikat?  
7. **IPS og IDS:** Funktion, teknologi, use cases, placering i netværk hostbaseret/netværksbaseret, signatur/anomali detektering, eksempler på regler


