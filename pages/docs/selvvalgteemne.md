---
hide:
  - navigation
---

# Selvvagte fordybelsesområde - Indlejredesystem sikkerhed
For min selvvagte fordybelsesområde har jeg valgte indlejrede systemsikkerhed fordi jeg synes at emnet er meget interessant. I datamatiker uddannelsen skulle vi slet ikke tænke på hvis computeren kunne køre softwaren fordi vores projekter brugte ikke særlig meget resourcer. Når man programmer for [indlejrede systemer](#hvad-er-et-indlejret-system), skal man tage højde for systemet begrænset resourcer. Et andet grund jeg synes [indlejrede systemer](#hvad-er-et-indlejret-system) er interessant er forbindelse mellem software verden og hardware verden. Typisk når man programmerer noget så forlader man ikke software verden men når man programmerer noget for [indlejrede systemer](#hvad-er-et-indlejret-system) kan man skrive kode for hardware.

# Raspbery Pi board og styrsystemet brugte til denne projekt
- Raspberry Pi Pico w
- Computer med Manjaro linux

# Mini-projekter
## Setup
### Picotool
Picotool er en CLI værktøj til at interagere med pico boardet. Dokumentationen anbefaler at man bruger denne værktøj.  
Udføre følgene kommandoer  
`git clone https://github.com/raspberrypi/picotool.git --branch master`  
`cd picotool`  
`mkdir build`  
`cd build`   
`export PICO_SDK_PATH=~/pico/pico-sdk`  
`cmake ../`  
`make`  
![MakePicotool](images/Fordybelsesområde/makePicotool.png)  
Her laver jeg et link til picotool i `/usr/local/bin` så jeg skal ikke skrive stien til programmet hver gang jeg skal bruge værktøjet.  
![SymbolicLink](images/Fordybelsesområde/SymbolicLink.png)  

### Klon SDK og eksempler
Jeg følger [Getting started with pico](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf) documentation og springer direkte til kapitel 2 fordi jeg vil gerne gør det manuelt.  
Første laver jeg en mappe der hedder pico med `mkdir pico` og `cd pico` for at bevæge ind i mappen.  
Jeg kloner pico-sdk med `git clone https://github.com/raspberrypi/pico-sdk.git --branch master`.  
Bevæge ind i mappen med `cd pico sdk`,  
Initilisere submodules med `git submodule update --init`.  
Gå ud af mappen `cd..`.  
Klon eksempler ned med `git clone https://github.com/raspberrypi/pico-examples.git --branch master`  

### Toolchain installaion
I dokumentation har de brugte debian eller ubuntu så jeg skulle finde Manjaros versionen af toolchain. Efter jeg brugte lidt tid til at google det fandte jeg hvad jeg skal bruge. Når jeg er usikker hvad programmet hedder eller hvis det findes i package manager søger jeg altid tid først med `sudo pacman -Ss <Package here>`. Hvis jeg kan se den installere jeg med `sudo pacman -S <Package here>`.  

Installation af [toolchain](#hvad-er-en-toolchain) til at [cross-compile](#hvad-er-en-cross-compiler) fra linux til bare-metal [ARM](#hvad-er-arm)  
![ARMcrosscompiler](images/Fordybelsesområde/ARMcrosscompiler.png)  
![ARMlib](images/Fordybelsesområde/ARMlib.png)  

## Blink
Når man kloner sdk ned får man også nogle eksempler med. Dette er blink programmet fra pico-examples.  
![BlinkSRC](images/Fordybelsesområde/BlinkSRC.png)  
I pico-examnples mappe lave en mappe der hedder build.  
`mkdir build`  
`cd build`  
også kør `cmake ..` for at oprette build directory  
![cmake](images/Fordybelsesområde/cmake.png)  
Build mapppe burde se sådan ud  
![BuildMappe](images/Fordybelsesområde/BuildMappe.png)  

 Der står lidt længere ned at pico er den default board så hvis man bruger noget andet skal man angive navnet af boardet. Jeg kørte kommandoen igen men denne gang med boardet. Jeg bruger pico w.  
![cmake](images/Fordybelsesområde/cmakePicoW.png)  
Ind i /build/blink mappen bruge `make -j4` kommandoen for at bygge programmet. `-j4` betyder at den skal gøre det med 4 cores.    
![MakeBlink](images/Fordybelsesområde/MakeBlink.png)  
Jeg fik en advarsel  
![MakeBlinkWarning](images/Fordybelsesområde/MakeBlinkWarning.png)  
Jeg brugte picotool til at load programmet i boardet med der skete ingen ting.  
![LoadBlink](images/Fordybelsesområde/LoadBlink.png)  

I advarslen stod der at blink kræver en LED så jeg troede at blink eksemplet prøvede at lys den forkerte LED så jeg kiggede på [raspberry Pi datasheet](https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf). Dokumentationen sadge at LED på pico w er ikke forbundet til en pin men til en GPIO så blink projektet skal modificeres. Der står LED(WL_GPIO0) på pinout diagrammet så jeg prøvede at ændre `const uint LED_PIN = PICO_DEFAULT_LED_PIN` på linje 13 til `const uint LED_PIN = WL_GPIO0` men det virkede ikke.  

![PicoWPinoutDiagram](images/Fordybelsesområde/PicoWPinoutDiagram.png)  

Ifølge [denne eksempel](https://github.com/raspberrypi/pico-examples/blob/master/pico_w/wifi/blink/CMakeLists.txt) skal den have wifi for at få adgang til GPIO. for at give pico w adgang til wifi skal man includere nogle cmake options `cmake -DPICO_BOARD=pico_w -DWIFI_SSID="Your Network" -DWIFI_PASSWORD="Your Password"`  

Det virkede stadig ikke så jeg kopierede cmake og blink.c så den lignede eksemplet fuldstændig men det virkede stadig ikke.  
![BlinkSRCv2](images/Fordybelsesområde/BlinkSRCv2.png)  
Jeg troede det var fordi jeg forståede ikke cmake så jeg prøvede at google problemet. Efter rigitg meget tid fandte jeg ud af at grunden til blink programmet ikke virkede er fordi jeg brugte picotools forkert. Efter jeg brugte `sudo cp blink.uf2 /media/run/pico` virkede den.  

[BlinkVideo](images/Fordybelsesområde/Blink.mp4)

## Binary disassembly
### setup og test af gdb
Jeg brugte gdb (GNU Debugger) til disassembly.  
Jeg brugte `sudo pacman -Ss gdb` og fandte 3 der kunne være relevante.  
![GDB search](images/Fordybelsesområde/GDBvalg.png)  
Jeg var usikker hvad jeg skulle bruge så jeg installerede alle 3  
For at teste gdb lavede jeg en lille assembly program der gemte tallet 13 i en register  
![HelloARM](images/Fordybelsesområde/HelloARM.png)  

Hvis der står `arm-none-eabi-` så betyder det at programmet er er compiled fra en linux host til en [ARM](#hvad-er-arm) target.  
Compile med nedstående kommandoer  
`arm-none-eabi-as HelloARM.asm -g -o HelloARM.o`  
`arm-none-eabi-gcc HelloARM.o -o HelloARM.elf -nostdlib`  

Det er vigtigt at inkludere `-nostdlib` fordi som default inkludere compiler nolge standard biblioteker de allerede har en `_start:` funktion.  

Jeg kan se at mange af de værktøjer der bliver brugte starter med `arm-none-eabi-` så jeg prøvede at bruge `arm-none-eabi-gdb`.  
Brug gdb med kommandoen `arm-none-eabi-gdb HelloARM.elf`  
![gdbHelloARM.elf](images/Fordybelsesområde/GDBHelloARM.png)  
Guiden jeg fandte sagde at man skulle skrive `disassemble main` men det virkede ikke for mig fordi jeg har ikke en main funktion men  jeg ved at jeg har en `_start:` funktion så jeg gættede at jeg skulle bruge `disassemble _start`.  

Jeg kan se at det er identisk til min assembly kode så jeg har konfirmerede at det virker.  
For at forstå lidt mere af koden prøvede jeg at finde ud af hvad `mov` og `r` er.  
`mov` betyder move så den flytter noget til en register. `r` beryder register så linje 9 af koden hvor der står `mov r0, #13` betyder flyt 13 til register 0.

### blink.elf disassembly
Jeg brugte kommandoen `arm-none-eabi-gdb blink.elf` for at disaasemble blink programmet. Nu kan jeg bruge `disassemble main` fordi jeg ved at der er en main funktion. Når man samligner de to billeder kan man bedre forstå hvad der sker. Man ka tydelig se når en funktion bliver kaldte. 

![blinkDisassembly](images/Fordybelsesområde/GDBblink.png)
![BlinkSRCv2](images/Fordybelsesområde/BlinkSRCv2.png)

### Buffer Overflow
#### Shell code
For at vise en [buffer overflow](#hvad-er-en-buffer-overflow) angreb har jeg skrevet et lille c program der bruger `strcpy()`. Programmet tager et argument og printer den til skærmen.  
![BuffOverSRC](images/Fordybelsesområde/BuffOverSRC.png)  

Her kør jeg programmet for at vise den.  
![BuffOverDemo](images/Fordybelsesområde/BuffOverDemo.png)  

Jeg har også lavede en [makefile](#makefile) for at gøre build processen nemmere  
![BuffOverMake](images/Fordybelsesområde/BuffOverMake.png)  

Jeg bruger `gdb BufferOverflow` på programmet også skriver jeg `disas main`. disas er det samme som disassemble. Her kan jeg se forskellen mellem [ARM](#hvad-er-arm) assembly syntaks og x64 assembly syntaks f.eks. funktion kalde i [ARM](#hvad-er-arm) er `bl` men her hedder den `call`.   
![disasBuffOver](images/Fordybelsesområde/disasBuffOver.png)  

Jeg kan se at `strcpy()` bliver kaldte på linje 15, hvor der står `call     0x1030 <strcpy@plt>`.

Her begynder det at være svært fordi i de guides jeg følger står der noget andet og de antager at man ved allerede lidt om assembly og hvordan man bruger gdb så mange af de ting jeg laver her er ren trial and error, hvor jeg har ikke rigitg en begrundelse for hvorfor jeg gør de ting jeg gøre fordi jeg ved ikke helt hvad jeg laver.  

Her sætter jeg en breakpoint efter `strcpy()` funktionen bliver kaldte.  
![BreakpointBuffOver](images/Fordybelsesområde/BreakpointBuffOver.png)  

Efter det bruger jeg kommandoen `run $(python -c "print('A'*256)")`. `run` kør programmet og `$(python -c "print('A'*256)")` bruger python for at printe 256 A'er. Det gøre jeg fordi på linje 6 af BufferOverflow programmet har jeg en array med størrelsen 256 så jeg burde ikke får en segmentation fault.  
![pythonBreakpoint](images/Fordybelsesområde/pythonBreakpoint.png)  

Jeg får en fejl her med min breakpoint. Jeg bruger kommandoen `info break` for at se min breakpoints og `del 1` for at slet den.  
![printANorWork](images/Fordybelsesområde/printANotWork.png)  

Jeg vidste ikke hvad jeg skulle gøre så jeg prøvede at forsæt mde at følg guiden. Jeg brugte `x/200xb $esp`. x betyde hexedecimal og b betyder binær. 200 er hvor mange hukommelsesblok jeg vil have.  
![CannotAccessMem](images/Fordybelsesområde/CannotAccessMem.png)  

Jeg fik en fejl igen der sagde cannot access memory. Jeg vidste stadig ikke hvad jeg skulle gøre så jeg forsat med guiden.  
![infoFunc](images/Fordybelsesområde/InfoFunc.png)  

`info func` giver en liste af alle funktioner.  Jeg fulgte guiden og brugte `disas main` igen og fik noget liste anderledes denne gang. Den der står nederst er den ny disassembly. Den øverst er den gammel. 
![disasBuffOver](images/Fordybelsesområde/disasBuffOver.png)
![disasBuffOver2](images/Fordybelsesområde/disasBuffOver2.png)

Jeg sat nye breakpoints.  
![NewBreakpoints](images/Fordybelsesområde/BreakStrAndlea.png)  

Jeg prøvede at kør programmet igen med `run $(python -c "print('A'*256)")` og det virkede. Jeg ved ikke hvorfor det er lidt anderledes den anden jeg bruge `disas main` og hvorfor jeg kan tilgå hukommelsen nu. Når jeg samligner mit program med guidens program så kan jeg se at jeg burde skrive `x/200xb $rbp` i stedet for `x/200xb $esp`  
![AccessMemWorks](images/Fordybelsesområde/AccessMemWorks.png)  
Her kommer der til at være meget trial and error fordi jeg skal overskrive return pointer og jeg ved ikke hvor den er henne eller hvordan den ser ud så jeg prøver mig bare frem indtil jeg ser et eller andet jeg kan bruge.  
A viser som 41 og det kan jeg ikke se så jeg gjorde igen med 272 A'er.  
![SIGABRT](images/Fordybelsesområde/SIGABRT.png)  

Jeg fik en SIGABRT signal men guiden fik en SEGSEGV signal så jeg vidste at jeg gjorde noget forkerte. Jeg googlede problemet og fandte en [stack overflow svar](https://stackoverflow.com/questions/29063020/how-to-exit-with-sigsegv-instead-of-sigabrt). Den sagde compileren har stack smashing protection og jeg skal compile med `-fno-stack-protector` så jeg redigerede min makefile.  
![BuffOverMake](images/Fordybelsesområde/BuffOverMakeStackProt.png)  

Fordi jeg fik en SIGABRT signal vidste jeg at der var en segmentation fault så jeg kørte programmet igen med 264 A'er.  
![SEGSEGV](images/Fordybelsesområde/SEGSEGV.png)  

Jeg prøvede igen men med 4 B'er  
![4Bsegmentaion](images/Fordybelsesområde/4Bsegmentation.png)  
Her kan jeg se fremskridt fordi jeg kan se 4 42 som er mine 4 B'er. Her tænkte jeg at det var cirka 264 hvor jeg skulle gøre noget.  
Jeg havde nogle problemer med hukommelsen igen men jeg kan se at jeg overskrev et eller andet i bunden.  
![Overwritten](images/Fordybelsesområde/Overwritten.png)  

Efter nogle forsøger kunne jeg tilgå hukommelsen igen. Her brugte jeg `run $(python -c "print('A'*256+'BBBB')")`  
![BreakpointWorksAndCanSee](images/Fordybelsesområde/BreakpointWorksAndCanSee.png)  

Her prøvede jeg at indsæt shellcode og få den til at eksekvær men jeg kunne ikke få den til at virke. Jeg er rimelig sikker at jeg er meget tæt på og kan gøre det hvis jeg havde mere tid.  
![shellcodeInsert](images/Fordybelsesområde/shellcodeInsert.png)  

#### Nemmere buffer overflow angreb

Jeg tænkte at jeg skulle prøve noget der er mere simpel så jeg ændret mit program lidt. Programmet tager en string som argument også tjekker den hvis passworden er rigtig, hvis ja, kalder den `Granted()` funktionen. Det jeg prøver på nu med [buffer overflow](#hvad-er-en-buffer-overflow) er at få programmet til at pege til `Granted()` funktion og eksekvær den uden den rigtig password.  
![BuffOverSRC2](images/Fordybelsesområde/BuffOver2/BuffOverSRC2.png)  

Her er den nye program disassembled.  
![disasmainBuffover2](images/Fordybelsesområde/BuffOver2/disasmainBuffOver2.png)  

Jeg gjorde det samme som før hvor jeg prøvede mig frem indtil jeg kunne se noget jeg kunne bruge og jeg fandte noget jeg synes var interessant.  

SegFault  
![returnPointer](images/Fordybelsesområde/BuffOver2/returnPointer.png)  
x/200xb $rbp  
![MemAccessPattern](images/Fordybelsesområde/BuffOver2/MemAccessPattern.png)  

Jeg kunne se sammenhænge mellem hvad der står i hukommelsen og rejlbeskeden. På linje 3 af billdet til højre dvs linjen hvor der står `0x7fffffffe280` står det samme ting. Mit mål nu er at præve at overskrive den for at hvad der sker.  

En interessant fejl var den her hvor der står cannot access memory. Jeg kunne mærke at jeg var tæt på.  
![CannotAccessMemory](images/Fordybelsesområde/BuffOver2/CannotAccessMemAddress.png)  

Jeg overskrevet fejlbeskeden igen.  
![Overwritten2](images/Fordybelsesområde/BuffOver2/RenewedVigor.png)  

Her prøvede jeg at indsæt en adresse for at for programmet til at hop rundt.
![StillDoesntWork](images/Fordybelsesområde/BuffOver2/StillDoesntWork.png)  

Noget der forvirede mig var hver gang jeg skrev e6 så vidste den som a6c3.  
![e6IsWeird](images/Fordybelsesområde/BuffOver2/e6IsWeird.png)  

Efter rigtig meget trial and error virkede det endelig. Jeg sat en breakpoint i `Granted()` funktionen så jeg kunne se hvis funktionen bliver kaldte.  
![Done](images/Fordybelsesområde/BuffOver2/YEEEEEEEEEEEEEEEEEEEEEEEEEES.png)  

#### Analyse af c kode
Man kunne grep en hel repository for at finde sårbar funktioner  
![grepFunc](images/Fordybelsesområde/BuffOver2/grepFunc.png)  

Der findes programmer til at finde sårbarheder i kode. Her henter jeg flawfinder for at analysere koden.  
![FlawFinderPackage](images/Fordybelsesområde/BuffOver2/FlawFinderPackage.png)  

Her finder flawfinder sårbarheder i koden  
![FlawFinder](images/Fordybelsesområde/BuffOver2/FlawFinder.png)  

Man kan beskytte buffer overflow angreber ved at undgå sårbar funktioner. Nedenunder er nogle forslag til at forbedre c kode.  
- gets() -> fgets()  
- strcpy() -> strncpy()  
- strcat() -> strncat()  
- sprintf() -> snprintf()  

### Verificering af integriteten med kryptografiske signatur
I denne eksempel viser jeg ide'en bag verificering med kryptografiske signatur med linux kernel som ikke er [firmware](#hvad-er-firmware) men konceptet ved et kan stadig bruges.  

Her downloader jeg en linux kernel image og signaturen  
![DownloadImgSig](images/Fordybelsesområde/Signatur/DownloadImgSig.png)  

Fik en fejl der sagde at der findes ikke en offentlig nøgle  
![NoPublicKey](images/Fordybelsesområde/Signatur/NoPublicKey.png)  

Jeg søgte efter den nyeste stabil linux kernel for at se hvad der vil ske men jeg fik den samme fejl  
![LatestKernelNoPublicKey](images/Fordybelsesområde/Signatur/LatestKernelNoPublicKey.png)  

Jeg fandte en mulig løsning på [stackoverflow](https://stackoverflow.com/questions/25074877/cant-check-signature-public-key-not-found) der siger jeg skal hent nøglen og importere den til min keyring. Efter lidt mere søgning fandte jeg [denne](https://www.kernel.org/signature.html) side der forklarede, hvordan man importere nøgler.  
![ImportGPGKeys](images/Fordybelsesområde/Signatur/ImportGPGKeys.png)  

Jeg prøvede igen med den første kernel og den virkede. Jeg kan se at den ikke er troværdig  
![UntrustedSignature](images/Fordybelsesområde/Signatur/UntrustedSignature.png)  

Nu prøver jeg igen med den seneste kernel hvor man kan også download signaturen. Jeg fik også en fejl here men det var ikke meningen at den skulle fejl.  
![LastestKernelUntrusted](images/Fordybelsesområde/Signatur/LastestKernelUntrusted.png)  

### OS command injection
Her er koden der er sårbar over for [OS command injection](#hvad-er-os-command-injection)  
![NoncompliantOSinjection](images/Fordybelsesområde/OSCommandInjection/NoncompliantOSinjection.png)  

Injection virkede ikke  
![TestInjection1](images/Fordybelsesområde/OSCommandInjection/test1.png)  

Efter lidt trial and error fandte jeg en forsøg der brøkede kun over `any_cmd:`  
![TestInjection2](images/Fordybelsesområde/OSCommandInjection/test2.png)  

Jeg brugte `less /etc/passwd` for at se hvis det virkede  
![attackerUser](images/Fordybelsesområde/OSCommandInjection/attackerUser.png)  

En måde man kan beskytte mod [OS command injection](#hvad-er-os-command-injection) er at erstatte `system()` funktionen på linje 15 med `execve()`.  

# Supplerende information

## Hvad er et indlejret system?  
Ifølge [Coursera](https://www.coursera.org/articles/embedded-systems), som er en virksomhed der tilbyder online kurser, et indlejret system er et system med en dedikerede funktion. Det kan være en opvaskermaskin, smartwatch eller en mobiltelefon. [OWASP](https://scriptingxss.gitbook.io/embedded-appsec-best-practices/preface) siger at der er mange måder at fortolke indlejret systemer. Det lyder som om der er ikke en standadiserede definition af idlejret system som alle er enig med.

## Hvad er en toolchain?
En toolchain er en kæde af speciliserede værktøjer(tools) der bliver brugte til automatisering af softwareudviklingings process. En værktøj kan f.eks. være en compiler eller en linker. En eksempel er compileren i [toolchain installation](#toolchain-installaion) afsnit.

### Hvad er toolchain hardening?
Toolchain hardening er håndtering af toolchains hvor formålet er at levere sikker og pålidelig kode. Det kan man gøre ved at fjerne usikker biblioteker som også minimere attack entry points. Der er 4 steder at kigge på når man skal harden en toolchain: configuration, preprocessor, compiler, og linker. Det er meget vigtigt at kigge på det i starten fordi det er meget svært eller umuligt at harden en executable efter man har distribuerede den. Der findes 3 måder at konfigurere et projekt på: auto-configuration med værktøjer, Man kan manuelt skriv en makefile, og endelig med en IDE (Integrated development environment).

#### Debug og release build
Der skal være mindste to konfigurationer, en for debug og en for release. Debug builds har mere debug information for udviklerne og release build skal have ingen eller meget lidt debug information. Man kan kontrollere debug information med `-g` flag i gcc. `-g0` er den laveste niveu som outputter ingen debug information. `-g1` producere minimal debug information. `-g2` er den default så hvis man skriver bare `gcc -g` så defaulter den til niveu 2. `-g3` er den højeste niveu.  

I [Buffer overflow](#buffer-overflow) afsnittet brugte jeg `-g3`  og det vil man ikke i en release build.  
![BuffOverMake](images/Fordybelsesområde/BuffOverMakeStackProt.png)  

#### Auto-configuration
Auto-configuration automatisere konfigurationsprocessen og er en populære måde at konfiguere projekter på men auto-configurations værktøjer har ikke sikkerhed som et mål og man skal ofte manuelt redigere i filen aligevel.  

#### Makefile
Make er en gammel build system som bliver stadig brugt. Make har nogle kendte sårbarheder som er et problem fordi der er mange der bruger den. Når man skal byg en debug og release version så vil make bygge debug og vil ikke ændre noget for release version så man kan ende med at udgive debug versionen. Hvis man bryger make så skal man være opmærksom på [CVE'en](https://www.cvedetails.com/vulnerability-list.php?vendor_id=72&product_id=663&version_id=0&page=1&hasexp=0&opdos=0&opec=0&opov=0&opcsrf=0&opgpriv=0&opsqli=0&opxss=0&opdirt=0&opmemc=0&ophttprs=0&opbyp=0&opfileinc=0&opginf=0&cvssscoremin=0&cvssscoremax=0&year=2000&cweid=0&order=1&trc=1&sha=7ea03639ccc8b43ac0a1d3bec3cdc02b7b1188cd).  

## Hvor bliver indlejret systemer brugt?  
Indlejret systemer bliver brugt i mikroprocessor og mikrokontroler. En mikroprocessor har kun en CPU hvorimod en mikrokontroller har også hukommelse og I/O. Indlejret systemer bliver også brugte i biler, mobiltelefoner, IoT og real-time miljø som bruger f.eks. RTOS som står for Real-Time Operating System.

## Hvad er managed og unmanaged kode?
[Microsoft](https://learn.microsoft.com/en-us/dotnet/standard/managed-code) siger at managed kode er kode der bliver styrede af en runtime. Common Language Runtime eller CLR kompilerer managed kode til maskinkode og kan eksekver koden. CLR har også automatic memory management, security boundaries, and type safety. Unmanaged kode har ikke en CLR så udvikleren har ansvaret for alt. Typisk arbejder man med begrænset resourcer når man arbejder med indlejret systemer så C bliver brugte rimelig ofte i indlejret systemer og fordi C er et unmanaged sprog kan den resultere i sårbar kode hvis man tager ikke højde for det.  

## Hvad er en cross-compiler?
Cross-compiling betyder at softwaren er lavede på en system men den skal kører på en anden system f.eks. [blink](#blink) projektet der er lavede på en x86-64 linux system men skal køres på et [ARM](#hvad-er-arm) system. Systemet der bygger softwaren hedder "build host" og systemet der skal køre softwaren hedder "target system" eller "target platform".  
![ARMcrosscompiler](images/Fordybelsesområde/ARMcrosscompiler.png)  

## Hvad er RISC?
RISC (Reduced Instruction Set Computer) er en mikroprocessor arkitektur der er mere simpel end CISC (Complex Intruction Set Computing) arkitekturen som inkludere x86 og x64 processerer. 

### Hvad er ARM?
ARM (Advanced RISC Machine) processerer bruger RISC arkitekturer og findes i f.eks. smartphones, tablets, laptops og mikrokontrollers.

## Hvad kan man bruge binnary disassembly til?
Binary assembly kan blive brugte til at analysere en binary. Grunden til man vil gøre det kan være hvis man ikke har adgang til kildekoden eller hvis man vil se hvad der sker i programmet helt nede i registerne. I [buffer overflow](#hvad-er-en-buffer-overflow) afsnittet lavede jeg en buffer overflow ved hjælp af binary disassemblt. En udviklere vil kunne bruge den til at test og finde softwarens sårbarheder.  

## Hvad er en buffer Overflow
En buffer overflow er når et process skriver mere data end hvad der er plads til i en buffer. Hvis det sker så vil dataen flyde over og overskrive data der er i nærheden af bufferen. I den nedenstående billedet er en diagram der viser en array med størrelsen 6. Hvis man prøver at skrive 7 værdier ind i arrayen så vil den overskrive dataen der er ved siden af arrayen. Det kan udnyttes ved at f.eks. overskirve return pointer og få den til at pege et andet sted  i programmet.  
![BuffOverDia](images/Fordybelsesområde/BuffOver2/BuffOverDia.png)  

## Hvad er firmware?
Ifølge [OWASP](https://scriptingxss.gitbook.io/embedded-appsec-best-practices/preface) firmware er software der er imellem hardware og styresystemet. Den kommunikere med hardwaren for at initialisere hardwaren for computeren og giver kontrol til styresystemet. Der findes forskellige typer af firmware så der findes også firmware der kan gøre mere. Kompromitteret firmware er meget svært og dyrt at finde.  

Der er to måder en trusselsaktør kan udnytte firmware på, Firmware-as-vehicle angreb og Firmware-as-agent angreb. Firmware-as-vehicle betyder at firmwaren er modificerede efter den er færdig med udvikling og skal ud til kunderne. Firmware-as-agent betyder at firmwaren bliver ikke modificerede men de findes eksisterende sårbarheder i koden som kan udnyttes. Hvis en trusselsaktør kan modificere firmware så kan de lave en back door til enheden og fordi firmware har meget høje privilegier kan det være skadelidt hvis en trusselaktør udnytter sårbarheder i firmware. 

## Opdatering af firmware med kryptografiske signatur
Fordi firmware er nødvendig for at interagere med hardwaren det er meget farligt hvis opdatatering fejler. Det er derfor at OEM (Original quipment manufacturer) pusher ikke opdateringer til kunderne men det er kunden der vil typisk beg om opdateringen og kun hvis der er en god grund til det. Der findes motherboards der kan DualBios så hvis der sker noget ved den ene firmware så har man en backup. For at opdatere firmware på en sikker akn man bruge krytogradiske signatur. Kryptografiske signaturer kan verificere integriteten af opdateringen.  

## Hvad er OS command injection?
OS command injection er et angreb hvor trusselsaktøren kan eksekvær system funktioner ved at udnytte sårbar inputs i programmer. 

# Kilder
- https://scriptingxss.gitbook.io/embedded-appsec-best-practices/6_embedded_framework_and_c-based_toolchain_hardeni  
- https://wiki.owasp.org/index.php/C-Based_Toolchain_Hardening  
- https://www.geeksforgeeks.org/advantages-and-disadvantages-of-arm-processor/  
- https://www.profolus.com/topics/arm-explained-advantages-and-disadvantages/  
- https://cmake.org/cmake/help/book/mastering-cmake/chapter/Cross%20Compiling%20With%20CMake.html  
- https://www.cvedetails.com/vulnerability-list.php?vendor_id=72&product_id=663&version_id=0&page=1&hasexp=0&opdos=0&opec=0&opov=0&opcsrf=0&opgpriv=0&opsqli=0&opxss=0&opdirt=0&opmemc=0&ophttprs=0&opbyp=0&opfileinc=0&opginf=0&cvssscoremin=0&cvssscoremax=0&year=2000&cweid=0&order=1&trc=1&-sha=7ea03639ccc8b43ac0a1d3bec3cdc02b7b1188cd  
- https://www.guru99.com/difference-between-microprocessor-and-microcontroller.html  
- https://learn.microsoft.com/en-us/dotnet/standard/managed-code  
- https://cwe.mitre.org/data/definitions/676.html#Demonstrative%20Examples))  
- https://www.arm.com/glossary/risc  
- https://scriptingxss.gitbook.io/embedded-appsec-best-practices/3_firmware_updates_and_cryptographic_signatures
- https://www.microsoft.com/en-us/security/blog/2020/02/03/guarding-against-supply-chain-attacks-part-2-hardware-risks/