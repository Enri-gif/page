

# Opgave 5 (Individuel øvelse) - Regular expression  

## Information
Formålet med denne øvelse, er at øge bevidstheden om tilgangen til opgave løsning, når
man står overfor en opgave der som udgangspunkt kan virke uoverskuelig, eller handler om et
emne som man endnu ikke er introduceret til.

**Alle hjælpe midler er tilladt**

## Instruktioner
I denne opgave skal i udarbejde et regular expression (Regex) som kan detekter mønsteret
for et dansk cpr nummer. Herefter skal i producerer et eksempel i C# som bruger Regex mønster, til
at verificere CPR numre.

_Når i har fundet et mønster, kan i teste det med [Regex 101](https://regex101.com/)_

([1-9]|[12][0-9]|3[01])([1-9]|1[0-2])([1-9]|[1-9][0-9]|90)\-\d\d\d\d

## Links
[Regex 101](https://regex101.com/)

# Øvelse 6 - Grundlæggende OOP med C sharp

## Information
Formålet med øvelsen er at sikre en grundlæggende forståelse for C# og de objekt orienteret programmering med denne.
Øvelsen tager udgangspunkt i en C# tutorial fra Microsoft, og en del af øvelsen er brugen af dokumentation.
  
Instruktionerne fungerer som en læse vejledning til tutorial, og fremhæver hvad du bør være særligt
opmærksom i hvert af afsnittene efter du har implementeret jvf. tutorial. Efter implementering af hvert afsnit, bør du læse 
vejledningen for hvert afsnit.

link til tutorial:  
[Tutorial](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/classes)

## Pratiske læringsmål med øvelserne
-   Den studerende oprette en klasse og forstår dennes anvendelse.
-   Den studerende anvende Constructor til initialisering af objekter.
-   Den studerende kan anvende Constructor til initialisering af objekter.
-   Den studerende har grundlæggende forståelse for Indkapsulering i objekter ved brug af private accessors.

## Instruktioner

### Prerequisites
I dette afsnit benævnes de grundlæggende værktøjer som kan anvendes til implementering
af software med C#. Visual studio er microsoft egen IDE(Integrated development enviroment), og
tilbyder en del støtte funktioner til implementering af software med C#. Visual studio code er
en text editor som også er udviklet af Microsoft. Denne kan udvides med en plugins, således at  
denne også kan fungere som IDE.  
SDK som nævnes er _Software Development kit_, og indeholder værktøjer som kræves for at udvikle
software med C#, F.eks. compiler (Roslyn), CLI værktøj (Dotnet) osv.  

### Create your application
I dette afsnit handler om oprettelse af projektet, som en konsol applikation.
I tutorialen anvendes `Dotnet new console` til at oprette projektet. Dette kan
også gøres i visual studio, som vist [her](https://learn.microsoft.com/en-us/visualstudio/get-started/csharp/tutorial-console?view=vs-2022)  
  
**Husk at projektet skal oprettes i en folder der hedder classes**  
  
### Define the bank acccount type
I dette afsnit skal du oprette filen _BankAccount.cs_ i projektet, og i filen
implementerer koden til klassen Bank Account. Overvej følgende punkter, efter implementeringen:

- Hvad er _Number_,_Owner_ og _balance_ ?  
properties  
- Hvad er _MakeDeposit_ og _MakeWithdrawal_ ?  
Motoder  
- Hvorfor omtales klasser også som typer?  
Klasser kan bruges som datatyper  

### Open a new account
I dette afsnit implementeres en construtor der tager imod 2 pararmetere. Herudover
implementeres en privat statisk variable. Overvej følgende punkter, efter implementeringen:
  
- Hvorfor implementer en constructor som tager imod 2 pararmetere?  
For at give objektet nogle varibler til start med  
- Kan objektet stadig initialiseres uden pararmetere?  
Ikke i dette tilfælde  
- Hvad betyder det at _s_accountNumberSeed_ er privat? (Kan variablen bruges uden for klassen, i F.eks. konsol applikationen?)  
privat fields kan kun bruges ind for klassen  

### Create deposits and withdrawals
I dette afsnit implementeres en ny type(klasse) _Transaction_. som herefter anvendes 
til udførelse deponering og udtræk. Overvej følgende  punkter, efter  implementeringen:
  
- Hvad er fordelen ved at transaktioner har sin egen type(klasse)?  
Single responsibility  
- hvorfor er listen _allTransactions_ private? (indkapsulering)  
Fordi folk må ikke pille ved den  
- Hvad sker der hvis koden ` throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive");` eksekveres?  
Så vil der kom en exception  
- Hvorfor skal metoden _MakeDeposit_ kaldes i _Constructor_?  
For at give en initial balance

### Challenge - log all transactions
I dette afsnit implementeres en metode som kan generer en tekst streng, som viser 
hele transaktions historikken. Overvej følgende punkter:  
- Hvordan anvendes den private liste _alltransactions_ , og hvorfor kan denne tilgang være en god ide?

# Øvelse 23 - Opretholdelse af objekters integritet med invariance i web applikationer

## Information
Formålet med denne øvelse er at vise hvordan Objekter der håndhæver deres invariance gennem en constructor kan anvendes i en web applikation,
til at sikre sikkerhed i flere lag.

Ofte vil forskellige mennesker arbejde på forskellige dele af en softwaren. Hvilket kan betyde at man måske tror at  noget (Såsom) input
validering bliver håndteret et andet sted. Uden at det faktisk gør det. Det kan gøre softwaren sårbart. Derfor bør man tænke defense in depth,
og sige at alt data softwaren modtager altid bliver valideret så hurtig som muligt, som du gjorde i Controller klassen i de forrige øvelser.
Men ligeså snart noget data bliver givet videre fra Controller klassen, til en anden klasse i software, så bliver det givet videre som et objekt
der opretholder invariance og derfor har integritet. Så opstår der aldrig tvivl om hvorvidt man kan stole på den data man modtager, internt i 
softwaren.

Typisk vil data som bliver afsendt af en anden applikation, have overskredet en tillidsgrænse og bliver derfor modtaget med en såkaldt `Data Transfer Object`(som i [øvelse 20](20_Input_validering_og_model_binding.md)). DTO har som ofte ikke nogen regler tilknyttet, udover hvilken type data den må indeholde. Derfor bør en dto aldrig forlade Controller klassen.
I denne øvelse vises der et (meget primitivt) eksempel på dette.

Du skal i denne øvelse orienterer dig om hvordan man anvender overgange fra et modtaget DTO objekt, til et objekt der håndhæver invarians.

Projektet du skal anvende er `InvarianceDTOandModel` fra repo'et [https://github.com/mesn1985/InputValidationsBasicExercises](https://github.com/mesn1985/InputValidationsBasicExercises),
som du klonet i en tidligere øvelse.

## Instruktioner
1. Inspicer metoden `ValidateName` i filen `NameController.cs`. hvorfor oprettes der et nyt objekt med værdierne fra det modtaget DTO objekt `person`?  

Det er for at afkoble frontend og backend for at undgå excessive data exposure

2. Åben filen `Person.cs` som kan findes i folderen _Models_
3. Implementer håndhævelsen af invariance for de to argumenter i Person constructoren. (Reglerne er de samme som fra tidligere øvelser)
4. Afprøv applikationen, og valider mod forskellige inputs.  
_Bemærk at den eneste regel input valideringen håndhæver er at data skal være sat [Notnull]_
5. Hvad sker der når du forsøger med F.eks. et navn på 21 karakterer?
Unhandled exception fejl

# Øvelse 28 - Detektering af sårbar afhæninger med Dotnet CLI værktøjet

## Information
Formålet med denne øvelse er at introducerer 3.parts sårbarheder og CVE'er.
Det meste software idag bygger på software andre har lavet tidligere. Dette importes
i projekter, og omtales ofte pakker, eller eksterne bibloteker.

Når man anvender eksterne bibloteker importer man noget kode som man ikke selv har 
udviklet og ikke nødvendigvis har hverken indflydelse eller indsigt i. Dette kan være
problematisk hvis der opstår en sårbarhed. Heldigvis findes der `CVE` databaser som indeholder
data om kendte sårbarheder i software. Et eksempel er [CVE.org](https://www.cve.org/).

I .Net importer man eksterne bibloteker som `Nuget packages`. Man kan se en liste over alle anvendte
eksterne bibloteker i projekts projekt file som altid slutter på `.csproj`.

I denne øvelse skal du anvende CLI værktøjet `Dotnet` til at skanne et .Net projekts `Nuget packages`
for sårbarheder.

Du skal klone repo [https://github.com/tobyash86/WebGoat.NET](https://github.com/tobyash86/WebGoat.NET) som
er et bevidst sårbart Asp .Net core projekt.

## Instruktioner
1. Gå ind i folderen til det klonet repository.
2. Følg instruksen fra denne [tutorial](https://devblogs.microsoft.com/nuget/how-to-scan-nuget-packages-for-security-vulnerabilities/) for at udføre en 3. Parts sårbarheds skanning(Se afsnittet dotnet CLI)
brugte kommandoen `dotnet list package --vulnerable` men fik en fejl der sagde jeg skulle restore projektet først.  
Det gjorde jeg med `dotnet restore` også prøvede jeg igen  
![CVEcli](images/SoftSik/CVEcli.png)  
3. Følg linket til den fundende sårbarhed, og noter CVE nummeret(i toppen af siden fra linket).
CVE-2022-41064  
4. Slå CVE'en op på [CVE.org](https://www.cve.org/). Kan der findes mere information om CVE'en?
https://www.cve.org/CVERecord?id=CVE-2022-41064
5. Brug `--include-transitive` for at se under sårbarhederne. (Se tutorial for yderlige information)
![CVEcliTransitive](images/SoftSik/CVEcliTransitive.png)  

# Øvelse 29
`Snyk test`  
![SnykTest](images/SoftSik/SnykTest.png)  

# Øvelse 30 - Grundlæggende taint analyse med Snyk.

## Information
Formålet med denne øvelse er at introducerer grundlæggende taint analyse med med 
værktøjet [Snyk](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli).

I opgaven skal du bruge udføre analysen på en ASP .Net core applikation. Repositoriet du skal 
klone er her: [https://github.com/brunobritodev/ASP.NET-Core-SQL-Injection-Example](https://github.com/brunobritodev/ASP.NET-Core-SQL-Injection-Example)  
Som navnet på applikationen indikerer, indeholder applikationen en sårbarhed der gør den modtagelig overfor injection angrebet _SQL Inject_.
Sårbarheden som vi skal arbejde med, er implementeret i Controller klassen `Homecontroller`, i metoden `SearchStudentUnsecure`.
Sårbarheden er meget åbenlys, hvilket gør den velegnet til at lære om taint analyser.

Taint analysen udføres ved at en algoritme følger en input værdi fra kilden (source) og til dens endelige mål(sink).
I applikationen er source, browserens input til applikationens, som ses i metoden ```public async Task<IActionResult> SearchStudentUnsecure(string name)```.
source må altså være argumentet ```name```. Sink i applikationen er databasen hvilket også kan ses i metoden `SearchStudentUnsecure` koden:
```Csharp
            var conn = _context.Database.GetDbConnection();
            var query = "SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'";
            IEnumerable<Student> students;

            try
            {
                await conn.OpenAsync();
                students = await conn.QueryAsync<Student>(query);
            }
```
Kode linjen `await conn.QueryAsync<Student>(query)` laver et kald til databasen med en SQL string som indeholder den 
ikke valideret data. Det vil sige at _tainted_ data nu får lov til at bevæge sige direkte fra source til sink.

I opgaven skal i efterprøve dette ved at anvende værktøj Snyk CLI som taint analyse værktøj.

Værktøjet er det samme i anvendte i [Opgave 29](29_Vulnerable_dependencies_Snyk.md).  
Du skal dog først oprette en konto og tænde for `Code scan` funktionaliteten. Følg guiden [her](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli)
Efterfølgende skal du blot følge trine i instruktionerne.

## Instruktioner
1. I det klonet projekts folder. Skift til folderen `SqlInjection`
2. udfør taint analysen ved at følge instruktionerne fra afsnittet `Testing a repository from its root folder` i denne [dokumentation](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli/testing-your-source-code-using-the-cli)
![SnykCode](images/SoftSik/SnykCode.png)  
3. Noter dig hvilke linje nummer SQL inject sårbarhed bliver fundet på.
32
4. Verificer i koden, at linje nummeret for sårbarheden er der hvor den forurenet data sendes til sink.
![SQLinject](images/SoftSik/SQLinject.png)  

# Øvelse 31 - Taint analyse med falsk positiv

## Information
Taint analysen vurderer om data er _tainted_ ved at følge input data gennem alle dets mulige vej i koden.
Hvis taint analysen ikke vurder at udvikleren som har skrevet koden har lavet en aktiv og valid foranstaltning
for at sikre at dataen er valid, vil dataen aldrig blive vurderet som _untainted_. Bliver den _tainted_ data lagt
sammen men noget anden data, vil den sammenlagte data nu blive betragtet som værende tainted. Et eksempel på dette
ses i koden fra forrige opgave:  
```Csharp
var query = "SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'";
```  
Som det ses sammentrækkes `name` med andre tekst strenge, og den resulterende tekst streng `query` bliver
derfor betragtede som værende _tainted_. Hvilket var det taint analyse reageret på i forrige afsnit.

En naturlig tilgang vil være at lave en input validering af name, ud fra den kontekst udvikleren vurder at 
dataen ikke længere er _tainted_. Men da taint analyse værktøjet som udgangspunkt ikke har nogen forudsætning
for at vurderer hvornår data bør betragtes som sikker i en kontekst bestemt situation, vil den stadig betragte
dataen som værende _tainted_, og derfor reagere. Dette kaldes en falsk positiv. Altså taint analyse vurder data
som værende forurenet selv det ikke er tilfælde.  
_Mange taint analyse værktøjer kan konfigures til ikke at reagerer i specifikke tilfælde, dette dækker vi dog ikke i undervisningen_

I denne øvelse skal du fremkalde en falsk positiv i taint analyse. Projektet der arbejdes med, er projektet fra forrige opgave.

Du skal implementer input validering i metoden `SearchStudentUnsecure`.

Fremgangsmåden for validerings metode er magen til den du anvendte i [Opgave 19](19_Input_validering_med_post_form.md)

## Instruktioner
1. I klassen `HomeController`, opret en privat metode til validering af en tekst streng. Teksten må kun indehold alfabetiske karakter.
2. I metoden `SearchStudentUnsecure` skal du nu bruge validerings metoden, til at valider tekst strengen _name_.   _Validerings metoden skal være den første metode der bliver kaldt_
3. Eksekver nu taint analysen med Snyk. (tilsvarende forrige opgave)
![AfterValidation](images/SoftSik/SnykCodeAfterValidation.png)
4. Noter om _SQL Injection_ stadig detekteres som en sårbarhed.

# Øvelse 32 - Overset sårbarhed

## Information
De fleste taint analyse værktøjer betragter data fra objekter som værende _untainted_ (I sprog som understøtter OOP). Dette kommer sig af  
at taint analyse værktøjet formoder at en _Constructor_ har været anvendt til at oprette objektet som indeholder dataen. Det er formodet praksis
at objekter håndhæver invarians i _Constructoren_ og derfor formodes data'en at være untainted. Dette skal efterprøves i denne opgave.

I projektet fra de to forrige opgaver skal du implementerer en klasse ved navn `Name`. Klasse skal have et _private  field_ til en tekst streng.
Klassens Constructor skal tage imod en tekst streng som argument, og initialiser tekst strengs _private field_.
**Det er vigtig at invarians undtagelsesvist ikke håndhæves, da øvelsen skal vise hvordan sårbarheder ikke opdages ved dårlige praksiser**
Klassen skal desuden havde en _public_ metode ved navn _GetName_, der return værdien fra _private field_ tekst strengen.
Udfør herefter opgaven ved at følge trine i instruktioner.

## Instruktioner
1. i metoden `SearchStudentUnsecure`, fjern eller udkommenter validerings metode fra forrige opgave.
2. i starten af metoden `SearchStudentUnsecure`, initialiser et 'Name' objekt, med argumentet _name_.
3. i tekst strenge sammentrækningen ```"SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'"``` skal du udskifte _name_ med et kald til GetName metoden op `Name` objektet.
4. Eksekver Taint analyse med Snyk.(Som i de to forrige opgaver).
5. Hvad er grunden til at sårbarheden ikke længere bliver fundet?
![SQLinjectionFixed](images/SoftSik/SQLinjectionFixed.png)  

# Øvelse 33 - Exception som angrebs vektor.

## Information
Formålet med denne øvelse er at introducerer faren ved manglende håndtering af exceptions i ASP .Net core MVC applikation.
Hvis du skal havde genopfrisket den grundlæggende brug af exceptions i .Net core, kan du med fordel kigge tilbage
på [Øvelse 21](./21_Throwing_Exceptions_In_CSharp.md).


Når en exception bliver kastet i en web applikation, er det vigtigt at den aldrig bliver synlig for slut brugeren(eller andre).
En exception indeholder typisk det der kaldes en _stack trace_ som viser hvilken kode der kastet exception, samt hele kæden
af metode kald der førte til kaldet af metoden, som kastet en exception. _Stack trace_ o.l. indeholder altså information om
softwarens eksekvering samt internt information om softwaren. Dette betyder at en stack trace er en potentiel angrebs vektor,
og kan derfor misbrugs af fjendtlige aktører.

Et godt eksempel på stack trace som angrebs vektor er sql Injection angreb. Typisk vil sådan et 
angreb starte med at angriberen prøver og fremprovoker en fejl i applikationen med enten `'` eller `;` karakter i et tekst felt.
Såfremt der opstår en fejl som følge af disse karakter er det meget sandsynligt at applikationen er sårbar overfor  SQL Injection 
angreb. Hvis den fremprovokeret fejl vises en SQL exception med stacktrace er applikationen helt sikkert sårbar overfor SQL Injection angreb, og
den fjendtlige aktør opnår samtid viden om koden, samt den del af koden som kastet en en exception.


I denne øvelse skal du først fremprovoker en fejl som resulterer i at der vises en stacktrace fra en SQLException. Og herefter skal du
arbejde med håndtering af exception i ASP .Net Core.

Projektet du skal arbejde og klone er [WebGoat](https://github.com/tobyash86/WebGoat.NET).

## Instruktioner
Først skal du verificerer at applikationen virker ved at fortag et køb,
når du så har verificeret applikationens almindelig virke, skal du fremprovokerer en SQL exception.

### Verificer at applikationen virker.
1. Start den klonet applikation.
2. Opret en bruger i applikationen.
3. Køb nogle vare i applikationen (tilføj vare til kurven og bestil dem).

### Fremprovoker en Sql Exception
1. Læg nogle vare i indkøbs kurven.
2. Gå til checkout
3. Udfyld alle felter i Check out. Men i feltet _Name to ship to_ skal du tilføje karakteren `'`.
![Apostrof](images/SoftSik/Apos.png)  
4. Klik _Place order_
5. Læs exceptionen og stacktrace som bliver vist. Hvilken information får du?
![StackTrace](images/SoftSik/WebGoatStackTrace.png)  
6. Stacktracen viser at linje 52 og linje 151 i kildekoden smed exceptionen. Prøv om du kan finde disse i kilde koden. Hvad er årsagen til at applikationen er sårbar?
7. Gentag trin 1 - 6. Denne gang med `;` i stedet for `'`.

I klassen __OrderRepository__ kan man se årsagen til sårbarheden i metoden _CreateOrder_.
```CSharp
 var sql = "INSERT INTO Orders (" +
                "CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, " +
                "ShipCity, ShipRegion, ShipPostalCode, ShipCountry" +
                ") VALUES (" +
                $"'{order.CustomerId}','{order.EmployeeId}','{order.OrderDate:yyyy-MM-dd}','{order.RequiredDate:yyyy-MM-dd}'," +
                $"{shippedDate},'{order.ShipVia}','{order.Freight}','{order.ShipName}','{order.ShipAddress}'," +
                $"'{order.ShipCity}','{order.ShipRegion}','{order.ShipPostalCode}','{order.ShipCountry}')";
            sql += ";\nSELECT OrderID FROM Orders ORDER BY OrderID DESC LIMIT 1;";
```
Der bliver anvendt sammentrækning af strenge fra _order_ objekt som er af typen Order. Et hurtig kig i Order klassen afslører 
at klassen ikke håndhæver invarians eller regler af nogen slags. Endevidere har metoden _Checkout_ i Controller klassen _CheckoutController_, som kalder _CreateOrder_ metoden i _OrderRepository_ klassen, ingen input validering. Input validering i Controller klassen, eller brug af  domæne primitiver i Order klassen (og dermed håndhævelse af invarians) ville have kunne mitigeret denne sårbarhed.

Sammentrækning af strenge bør aldrig anvendes til at oprette SQL Forspørgelse. Såfremt det ikke kan undgåes, skal der anvendes [Parameterized statement](https://learn.microsoft.com/en-us/aspnet/web-forms/overview/data-access/accessing-the-database-directly-from-an-aspnet-page/using-parameterized-queries-with-the-sqldatasource-cs). Men den bedste praksis er anvendelse [Object relationel mapper](https://learn.microsoft.com/en-us/dotnet/architecture/modern-web-apps-azure/work-with-data-in-asp-net-core-apps)(ORM) som helt undlader
brugen af tekst strenge.
  
Øverige dele af kildekoden anvender ORM til interaktion med databasen, Nedstående er der vist et eksempel fra metoden _CreateorderPayment_.
```CSharp
var orderPayment = new OrderPayment()
            {
                AmountPaid = Convert.ToDouble(amountPaid),
                CreditCardNumber = creditCardNumber,
                ApprovalCode = approvalCode,
                ExpirationDate = expirationDate,
                OrderId = orderId,
                PaymentDate = DateTime.Now
            };
            _context.OrderPayments.Add(orderPayment);
            _context.SaveChanges();
```
  
ORM bør altid anvendes til interaktion med databaser, fra kildekoden. Der kan undtagelsesvist i meget få tilfælde være en grund
til at undlade at anvende ORM. Dette er dog meget sjældent.


# Øvelse 34 - Håndtering af exception i ASP .Net Core Applikationer

## Information
Formålet med denne øvelse, er at vise den grundlæggende tilgang til overordnet håndtering af exceptions
i applikationer.

Øvelsen viser blot den overodnet og generiske tilgang til at sikre, at applikationen ikke uhensigtmæssigt
eksponerer en stack trace. Men ikke hvordan man gennemgående skal håndterer exceptions, da dette emne 
vil blive for stort ift. emnets størrelse i faget. Såfremt man ønsker at lære mere om håndtering af 
uventede fejl (Exceptions) kan man referrer til programmeringssproget og frameworkets dokumentation,
F.eks. til MS docs ASP .Net core [Handling exceptions in ASP .Net Core](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/error-handling?view=aspnetcore-7.0).

Tilgangen  der skal arbejdes med referes ofte som _Global exception handling_. Dette betyder at alle exceptions som ikke bliver grebet(med F.eks. en Try/catch statement)
altid vil blive håndteret af en generisk exception handler, der typisk returner en generisk fejl tilbage til slut brugeren (F.eks. HTTP status kode 500 i web applikationer).

I [WebGoat](https://github.com/tobyash86/WebGoat.NET) applikationen er der implementeret en global håndtering af exceptions. Dette kan ses 
i metoden _Configure_ i _Startup_ klassen. i nedstående udsnit er dette vist.

```CSharp
if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
else
    {
        app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");
    }
```
I forrige var det muligt at fremprovokerer en stacktrace fordi applikationen eksekverer i udviklings miljø. Dette er belejligt når udvikler
skal ændre på software, for så at kunne se de konkrette fejl meddelelser. Men i et produktions miljø er dette ikke hensigtsmæssigt, da det som vist
i forrige opgave, vidergiver information til potentielle fjendtlige aktører.

*Det er vigtig at understrege, at global fejl håndtering er en sidste "nød bremse" i tilfælde af uforudset fejl, der ikke bliver håndteret af applikationen*.
Kan en angriber fremprovokere en status 500 meddelelse, vil dette stadig indikerer overfor angriberen at der er blevet fremprovokeret en fejl. Angriberen
kan dog ikke se hvilken fejl, eller udlæse informationener fra stacktrace.

For yderlige informationer om fejlhåndtering kan du læse MS DOCS [Handling exceptions in ASP .Net Core](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/error-handling?view=aspnetcore-7.0), eller (gen)læse kapitel 9 i bogen _Secure by Design_.


## Instruktioner
1. I klassen _Startup_ skal du udkommenterer koden for global håndtering af exceptions(tidligere vist i afsnittet _Information_), og i stedet indsætte `app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");`
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;` (samme fremgang måde som i forrige øvelses).
3. Noter dig hvilken fejl der opstår? Giver den fejl mening? Og er den fejl valgt fremfor F.eks. Status kode 500?
![404](images/SoftSik/WebGoat404.png)  

Fejl meddelsen er ikke korrekt, og ikke særligt sigende. Men her er det vigtig og huske, at den globale håndtering af exceptions kun er en nødløsning til de 
forhåbentlige få tilfælde af exceptions der ikke bliver grebet i en applikation. Altså exceptions bør som udgangspunkt være håndteret i koden, og opstår kun
i de tilfælde hvor noget uventet går galt. Hvis en domæne primitiv F.eks. kaster en Exceptions, indikerer det at input valideringen ikke har fanget noget dårligt data,
der er blevet sendt over fra klienten. Og ift. til almindelige bruger (der ikke har ondsindet intentioner), bør klient side valideringen(F.eks. i browseren) allerede havde forhindret brugeren
i at sende den dårlige data.  
  *Husk klient side validering er ikke en sikkerheds mæssig foranstaltning!, blot en indikator til brugeren om at data ikke overholder formatet*.  
Man bør tænke _defense in depth_ når man tænker håndtering af exceptions. Og global exceptions håndtering er det sidste lag i denne kontekst. Dette 
forhindre at en stack trace ikke uhensigtsmæssigt bliver eksponeret til uvedkommende. Men angriberen kan dog stadig se at inputtet udløser en fejl.  
  
I de næste trin af øvelsen skal du helt fjerne den globale fejl håndtering.

1. I i metoden `Configure`, i klassen _Startup_, skal du udkommenterer `app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");`
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;`
3. Noter dig hvilken fejl der opstår? Hvilken HTTP status kode bliver der retuneret? 
![Blank](images/SoftSik/WebGoatBlank.png)  

ASP .Net Core understøtter princippet om _Secure by default_. Dette betyder at hvis en uhåndteret fejl opstår uden at blive
grebet (Ej heller af global exception håndtering), så bliver status koden 500 sendt tilbage til klienten. Altså den viser ikke 
stacktracen som udgangspunkt (Dette kan man dog godt opleve i andre frameworks).

I den sidste øvelse skal _Developer exceptions pages_ anvendes.
  
1. I i metoden `Configure`, i klassen _Startup_,skal du indsætte koden `app.UseDeveloperExceptionPage();` i starten af koden.
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;`
3. Noter dig hvilken fejl der opstår?
![StackTrace](images/SoftSik/WebGoatStackTrace.png)  

Developer exception code er en bekvemlighed for udvikler der udvikler på applikationen, Dette bør kun anvendes i 
Udviklings miljø'er. Det hænder dog nogen gange at dette ved en fejl også bliver brugt i produktions miljøer, F.eks.
på grund at forkerte [miljø variabler](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-7.0).

# Øvelse 36 - Test af invarians.

## Information
Formålet med denne opgave at introducerer unit testing af objekters håndhævelse af invariance.

I opgaven skal du implementer unit tests til domæne primitiverne _Firstname_ og _Lastname_ som du implementeret i
[Opgave 26](./26_Person_with_domain_primitivesWebApplication.md).

Den metode som skal tests er constructor kaldet. Du kan se et eksempel på validering af kastet exception [her](http://dontcodetired.com/blog/post/Testing-for-Thrown-Exceptions-in-xUnitnet). Og et eksempel på validering af at der ikke bliver kastet exception [her](https://peterdaugaardrasmussen.com/2019/10/27/xunit-how-to-check-if-a-call-does-not-throw-an-exception/).
  
Når vi skal arbejde med build pipelines i en senere lektion, kigger vi på hvordan unit tests kan hjælpe med at opretholde sikkerheden i software.
     
I opgaven skal du huske og flytte projektet fra [Opgave 26](./26_Person_with_domain_primitivesWebApplication.md), ind i et biblotek, og tilføj en solution, ligesom
i tutorialen i forrige opgave. 
  
## Instruktioner
1. Opret en test klasse til _Firstname_ domæne primitivet
2. Lav i test klassen, en test metode der validerer at der *ikke* bliver kastet en exception når reglerne for lægenden på navnet bliver overholdt.
![NoExceptionCode](images/SoftSik/CodeNoException.png)  
![NoExceptionCode](images/SoftSik/PassedNoException.png)  
3. Lav i test klassen, en test metode der validerer at der *ikke* bliver kastet en exception når reglerne for hvilken karakter navnet må indehold bliver overholdt.
4. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for længden på navnet ikke bliver overholdt. (For mange eller for får karakterer)
5. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for hvilken karakter navnet må indeholde ikke bliver overholdt(test både med tal og speciel karakter)
6. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når objektet bliver initialiseret med null.
  
Input validering i Controller klasserne kan også unit tests, dette kan du læse mere om [her](https://learn.microsoft.com/en-us/aspnet/core/mvc/controllers/testing?view=aspnetcore-7.0)

