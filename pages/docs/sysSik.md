# System sikkerhed

## Læringsmål:

### Viden
Den studerende har viden om:
- Generelle governance principper / sikkerhedsprocedurer
- Væsentlige forensic processer
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- Sikkerhedsadministration i DBMS.

### Færdigheder
Den studerende kan:
- Udnytte modforanstaltninger til sikring af systemer
- Følge et benchmark til at sikre opsætning af enhederne
- Implementere systematisk logning og monitering af enheder
- Analysere logs for incidents og følge et revisionsspor
- Kan genoprette systemer efter en hændelse.

### Kompetencer
Den studerende kan:
- håndtere enheder på command line-niveau
- håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikkeit-sikkerhedsmæssige hændelser.
- håndtere relevante krypteringstiltag

<details>
	<summary>Noter</summary>  
OS API  
Systemstrace  
ntp - network time protocol  
Active directory  
LDAP  
PAM(LINUX)  
CIA-T, T for traceability  
Log når der er en sikkerhedskrav og compliance  
</details>



## Uge 6

Installerede Ubuntu på VMware

## Uge 9
https://ucl-pba-its.gitlab.io/exercises/system/9_User_accounts/#filer-tilknyttet-en-bruger

POLP  

### Opgave 9

Tilføj bruger  
sudo useradd Mandalorian --force-badname  

Ændre bruger  
chage -E 2023-06-23 Mandalorian  
chage -l Mandalorian  

Ændre Home directory  
sudo mkdir ../mandoFiles/  
sudo usermod -d /home/mandofiles/ Mandalorian  

su - Mandalorian  

pwd  

I denne opgave skal følgende trin udføres:  
1. opret tre filer med brugeren Mandalorian.  
sudo chown Mandalorian mandoFiles  

touch file1.txt  
touch file2.txt  
touch file3.txt  

2. find dem med find kommandoen, eksekver søgning fra /.  
cd /  
find / file.txt -user Mandalorian  
![FindWithUser](images/FindWithUser.png)  

I denne opgave skal brugeren Mandalorian slettes, udfør opgaven som følgende.  

1. slet brugeren Mandalorian med userdel.  
&nbsp;&nbsp;&nbsp;&nbsp;Brugte sudo userdel Mandalorian
2. Find en af de filer som blevet oprettet af brugeren Mandalorian  
&nbsp;&nbsp;&nbsp;&nbsp;Brugte ls -la i mandoFiles mappen
3. Se rettighederne for denne file, og noter hvem der er file ejer.  
![FileOwnerDeleted](images/FileOwnerDeleted.png)  
4. Opret en bruger ved navn Ivan.  
&nbsp;&nbsp;&nbsp;&nbsp;sudo useradd Ivan --force-badname  
5. Gentag trin 3(Samme file)  
![IvanFiles](images/IvanFiles.png)  
6. Vurder om dette er en potetienelle sårbarhed, og om man bør overeje altid at slette/skifte ejerskab på filer når en bruger slettes  
Et alternativ til at slette bruger kontoer, er at deaktiver dem  
&nbsp;&nbsp;&nbsp;&nbsp;Det er muligt at få adgang til filer man skal ike have adgang til.  
7. Overvej om princippet Secure by default reelt er overholdt  

I denne opgave skal du udforske passwd filen.  

1. Se rettighederne for passwd med kommandoen ls <filenavn> -al.  
![passwd](images/passwd.png)  
2. Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.  
&nbsp;&nbsp;&nbsp;&nbsp;Det giver mening at root ejer filen  
3. Udskriv filens indhold Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7  
&nbsp;&nbsp;&nbsp;&nbsp;cat /etc/passwd  
![passwdContent](images/passwdContent.png)  
4. Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)  
Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):  
Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell  

I opgaven skal du udforske filen shadow.  

1. udskriver rettighederne for filen shadow  
![shadow](images/shadow.png)  
2. Overvej rettighederne i samhold med Privilege of least princippet.  
3. Udskriv filens indhold.  
![shadowContent](images/shadowContent.png)  
4. Samhold filens indhold med nedstående format.  
:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:  
Maksimum dage ind password skift:Varslings tid for udløbet password:  
Antal dage med udløbet password inden dekativering af konto:konto udløbs dato  

Alle kommandoer skal eksekveres mens du er i dit hjemme directory  
1. installer værktøjer john-the-ripper med kommandoen sudo apt install john  
2. Her efter skal du downloade en wordlist kaldet rockyou med følgende kommando  
wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt.  
3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password  
4. Hent nu det hashet kodeord for misternaiv med følgende kommando sudo cat /etc/shadow | grep misternaiv > passwordhash.txt  
5. udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.  
![passwordHashContent](images/passwordHashContent.png)  
6. Eksekver nu kommandoen john -wordlist=rockyou.txt passwordhash.txt.  
![NoPasswordLoaded](images/NoPasswordLoaded.png)  
7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.  
8. Eksekver nu kommandoen: john --format=crypt -wordlist=rockyou.txt passwordhash.txt.  
format fortæller john the ripper hvilken type algoritme det drejer sig om  
9. Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.  
![PasswordLoaded](images/PasswordLoaded.png)  


1. gentag processen fra trin 1 af. Men med et stærkt password.(minnimum 16 karakterer, både store og små bogstaver, samt special tegn)  
![HardPassword](images/HardPassword.png)  
Jeg stoppede den efter 5 minutter. Koden var HardPasswordsAreHard123!
2. Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).  

### Opgave 10

Udfør alle opgaver ved at tildele med både tal og bogstaver  
Giv og fjern læse rettigheder til en file  

I denne opgave skal oprettes en file, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.  
Giv og fjern skrive rettigheder til en file  

I denne opgave skal oprettes en file, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.  
Giv og fjern læse rettigheder til et directory  

I denne opgave skal oprettes en directory, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.  
Giv og fjern skrive rettigheder til et directory  

I denne opgave skal oprettes en directory, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.  
Giv og fjern eksekverings rettigheder til en file  

I denne opgave skal oprettes en file, og gives eksekverings rettigheder til gruppen. Fjern derefter rettigheden igen.  
Ændre ejeren af en file.  

I denne opgave skal ejerskabet over en file ændres til en anden bruger, med kommandoen chown.  
Sårbarhed - For mange rettigheder  

Reflekter over konsekvens ved for mange rettigheder. Hvorfor sker det nogen gange? Og hvorfor søger nogen ikke ondsindet bruger at få flere rettigheder?  

### Opgave 11

1. hent applikations filen martins_very_non_suspicous_app på Its learning, og unzip den.  
2. sørg for at filen er på din linux VM.  
3. sæt root brugeren som ejer af filen.  
4. giv root brugeren alle rettigheder til filen og sæt User ID bit. Gruppen og alle andre skal blot havde eksekverings rettigheder.  
![MartinsAppPermissions](images/MartinsAppPermissions.png)  
5. Skift til din almindelig bruger.  
6. ekserkver applikationen.  
7. skriv whoami  
![ExecuteApp](images/ExecuteApp.png)   

## Uge 11

Information

I Linux har to log systemer. rsyslog som er en nyere udgave af syslog, og journalctl.
I de kommende opgaver kigger vi på rsyslog, og log filerne i ubuntu (debian).

Log filerne opbevares i /var/log/.
Instruktioner
primærer log file

Den primær log file for systemet hedder syslog, og indeholder information om
stort alt systemet foretager sig.

1. udskriv inholdet af denne file.
![syslogDebian](images/syslogDebian.png)   
2. studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet.
&nbsp;&nbsp;&nbsp;&nbsp;
3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?

Authentication log

Log filen auth.log indeholder information om autentificering.

## Opgave 12

1. udskriv indholdet af auth.log
![authlogDebian](images/authlogDebian.png)   
2. skrift bruger til F.eks. root
3. udskriv indholdet af auth.log igen, og bemærk de nye rækker
![authlogDebianRoot](images/authlogDebianRoot.png)   
4. skrift tilbage til din primær bruger igen (som naturligvis ikke er root)
5. udskriv indholdet af auth.log igen, og bemærk de nye rækker

## Opgave 13  
1. Brug locate til at finde alle filer med ordet rsyslog  
2. Dan dig et generelt overblik over filerne. Er der mange tilknyttet filer?  
![locateRSYSLOG](images/locateRSYSLOG.png)   

Rsyslog konfigurations file.

Rsyslog log konfiguration filen indeholder den generelle opsætningen af rsyslog daemon, blandt andet hvem der ejer log filerne, og hvilken gruppe der er tilknyttet log filerne. Herudover har den module opsætning. Moduler er ekstra funktionalitet som man kan give til rlogsys.

1. brug locate til at finde rsyslog filen rsyslog.conf  
![locateRSYSLOG.conf](images/locateRSYSLOGconf.png)   
2. åben filen med nano  
3. I konfigurations filen, find afsnittet Set the default permissions for all log files.  
4. Noter hvem der er file ejer, og hvilken gruppe log filerne er tilknyttet.  
5. Udforsk de andre områder af filen.  

## Opgave 14
50-default.conf mangler  

## Opgave 15  

Information  

Når der løbende bliver indsamlet logs, vil log filerne typisk kunne blive meget store henover tid, og optage meget plads på lagrings mediet. For at undgå dette kan man roterer logs i et givet tidsrum. F.eks. kan en log roteres hver 3. Uge. Hvis log filen auth.log roteres vil, den skifte navn til auth.log.1 og en ny file ved navn auth.log vil blive oprettet og modtag alle nye logs.  

auth.log.1 bliver en såkaldt backlog file.  
Instruktioner  

I disse opgaver skal der arbejdes med log rotation i Linux (Ubuntu).  

Du kan finde hjælp i Ubuntu man page til logrotate.  

1. åben filen /etc/logrotate.conf  
![Logrotateconf.png](images/Logrotateconf.png.png)   
2. Sæt log rotationen til daglig rotation  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg ændrer weekly på linje 6 til daily  
3. sæt antallet af backlogs til 8 uger.  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg ændrer rotate 4 på linje 9 til roate 8  

Yderlige kan du prøve at sætte logrotationen op så den følger CFCS vejledning.  

## Opgave 16  
Information  

Logging daemons såsom rsyslog kan lukkes ned, på lige fod med andre applikations processer i Linux. Dette skal udføres i denne opgave  
Instruktioner  

1. Eksekver kommandoen service rsyslog stop Efter dette vil der ikke længere blive genereret logs i operativ systemet.  
![stoprsyslog](images/stoprsyslog.png)   
2. Eksekver kommandoen service rsyslog start  
![startrsyslog](images/startrsyslog.png)  
Typisk er den kun superbrugerer som kan lukke ned for en log service. Det betyder at hvis en angriber kan lukke ned for logging servicen, kan han også lukke ned for evt. sikkerheds mekanismer som er i samme operativ system. Overvej hvordan man kan undgå dette? med F.eks. log overvågning via netværk.  

## Opgave 17

Information  

I denne opgave er den nogle spørgsmål som kan hjælpe dig med at samle dit overblik over log systemet i Linux. Nogle af spørgsmålene referer til rsyslog, mens andre referer til Journalctl log systemet. Du vil formodentlig komme ud for at skulle søge svaret online, hvilket er en del af opgaven.  
Instruktione  

1. Hvad hedder den rsyslog file som indeholder alle begivenheder som relaterer til autentificering?  
&nbsp;&nbsp;&nbsp;&nbsp;auth.log
2. Hvilken log file indeholder oplysninger om bruger der er logget ind på nuværende tidspunkt?  
&nbsp;&nbsp;&nbsp;&nbsp;auth.log
3. Hvilken log system eksisterer på næsten alle moderne Linux systemer?  
&nbsp;&nbsp;&nbsp;&nbsp;

## Opgave 18  

Information  

I denne opgave skal der arbejde med applikations logs.  
Instruktioner  

1. installer apache2 web server med kommandoen sudo apt-get install apache2  
2. Verificer at servicen er startet med kommandoen systemctl status apache2  
![apache2Status](images/apache2Status.png)  
3. I en browser, gå ind på url'en . Verificer at det er apache's default side der kommer frem  
&nbsp;&nbsp;&nbsp;&nbsp;Jeg kan tilgå hjemmesiden så det virker.  
4. Udskriv indholdet af apache's adgangs log som findes i /var/log/apache2/access.log  
![Apache2Accesslog](images/Apache2Accesslog.png)  
5. Verificerer at den sidste log i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre trin 3  
6. Eksekver kommandoen tail -f access.log  
7. Udfør trin 3 et par gange igen, og verificer at der kommer en ny log entry hvergang.  
![Apache2Accesslog2](images/Apache2Accesslog2.png)  

Mange af de tidligere opgaver har været centeret om log filer fra operativ systemet. Men det er meget vigtig at huske, at applikationer ofte har sine egene log filer (og autentificering). Og at hver applikation eksekveres med et sæt bruger rettigheder.  

## Uge 13
### Opgave 26

Instruktioner

1. Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen apt install auditd  
2. Verificer at auditd er aktiv med kommandoen systemctl status auditd. Auditd active  
![AuditdStatus](images/AuditdStatus.png)  

3. brug værktøjet auditctl til at udskrive nuværende regler med kommandoenauditctl -l No rules auditd  
![AuditNoRules](images/AuditNoRules.png)   

4. Udskriv log filen som findes i /var/log/audit/audit.log auditd lof  
![AuditLog](images/AuditLog.png)   

Hvis loggen virker uoverskuelig, har du det tilfælles med næsten resten af verden. Derfor bruger man typisk to værktøjer benævnt ausearch og aureport til at udlæse konkrette begivenheder. Begge værktøjer arbejdes der med i de kommende opgaver.

At arbejde direkte med auditd er en smule primitivt ift. til mange andre værktøjer. Men det giver en forståelse for hvordan der arbejdes med audit på (næsten) laveste nivevau i Linux. Mange større og mere intuitive audit systemer (F.eks. SIEM systemer) benytter sig også af auditd, eller kan benytte sig auditd loggen.

### Opgave 27

Instruktioner
Tilføj audit regel med auditctl`

1. Opret en audit regel for hver gang der skrives eller ændres på filen /etc/passwd med kommandoen auditctl -w /etc/passwd -p wa -k user_change

Muligheden -w står for where og referer til den file hvor audit reglen skal gælde. -p står for permission og fortæller hvilken rettigheder der skal overvåges. I eksemplet står der wa hvilket står for attribute og write. Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log
![AuditPasswdRule](images/AuditPasswdRule.png)   

2. udskriv en rapport over alle hændelser der er logget med kommandoen aureport -i -k | grep user_change

Muligheden -i står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. -k står for key, hvilket betyder at regel der udløst audit loggen skal vises

3. tilføj noget text i bunden af filen fra trin 1.
4. Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten
Der kommer en del nye rækker
5. Brug kommandoen ausearch -i -k user_change. Her skulle du gerne kunne finde en log som ligner den nedstående.
Ausearch
Generelt kan det være en smule rodet at skulle finde rundt i disse logs. Oftest bliver der produceret mere end en log når en begivenhed udløser en regel. Forskellen på ausearch og aureport er at ausearch viser flere oplysninger, men dermed også bliver mere uoverskuelig.
Tilføj audit regel med audit regel konfigurations file

Ulempen ved at tilføje regler direkte med auditctl er at regler ikke bliver persisteret. Altså de bliver ikke gemt og forsvinder når auditd eller hele systemet bliver genstartet. For at gemme reglerne skal man lave en regel file og gemme den i /etc/audit/rules.d/

1. Åben filen /etc/audit/audit.rules. Den skulle gerne se ud som vist nedenunder. Audit rule file
regel filen bliver indlæst ved opstart af auditd og alle regler heri bliver de gældende audit regler

Dette er auditd's primær konfigurations file, som indeholder alle regel konfigurationerne. men bemærk kommentaren øverst i filen. Den fortælle at denne file bliver autogeneret ud fra indholdet af /etc/audit/audit.d directory (ikke en synderlig intuitiv måde at lave konfigurations opsætning på).
2. Opret en ny file som indeholder reglen, med kommandoen sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"
For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med auditctl -l inden trin 2 udføres
3. Genstart auditd med kommandoen systemctl restart auditd
4. Udskriv indholdet af /etc/audit/audit.rules. Resultatet bør ligne det som vises på billedet nedenunder.
Audit config file with rule

Slet filen /etc/audit/rules.d/custom.rules efter øvelsen
Audit alle file ændringer.

Man kan også udskrive alle ændringer der er blevet lavet på overvåget filer.

    Eksekver kommandoen aureport -f. Med denne bør du få en udskrift der ligner nedstående.
    All files audit

Den første række med nummeret 1. blev indsat 27 Marts 2023. kl. 20:11. begivenheden som udløste loggen var en udskrift af filen tmp.txt. udskrivning blev lavet af brugeren med id 1000. Og udskrivningen blev eksekveret med kommandoen cat
2. Ændre kommandoen fra trin 1 således den skriver brugernavn i stedet for bruger id

# Uge 17

## Opgave 32

## Opgave 33
Opsætning af Wazuh agent

Alle kommandoer eksekveres på den overvåget host
1. opret directoriet /home/SecretFolder  
kør kommandoen ```mkdir SecretFolder``` på ubuntu agenten  

2. Åben Wazuh agentens konfiguration file i en tekst editor. Filen findes i /var/ossec/etc/ossec.conf  
```vim /var/ossec/etc/ossec.conf```  

3. I blokken <syscheck> skal følgende block tilføjes <directories check_all="yes" report_changes="yes" realtime="yes">/home/SecretFolder</directories>
4. genstart wazuh agenten med kommandoen systemctl restart wazuh-agent
5. opret filen /home/SecretFolder/secretFile.txt.
6. Tilføj teksten Bad mojo til filen /home/SecretFolder/secretFile.txt
```vim /home/SecretFolder/secretFile.txt```  
tilføj teksten ```Bad mojo```  

7. Slet filen /home/SecretFolder/secretFile.txt 
```rm SecretFolder/secretFile.txt```  

## Opgave 34

## Opgave 35

## Opgave 36

## Opgave 37

# Uge 1
## Opgave 38

# Uge 21

## Opgave 45

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-32269  
https://nvd.nist.gov/vuln/detail/CVE-2023-32269  
Medium  

https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=CVE-2023-31436  
https://nvd.nist.gov/vuln/detail/CVE-2023-31436  
High  

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0160  
https://nvd.nist.gov/vuln/detail/CVE-2014-0160  
Heartbleed  
High

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-47509  
https://nvd.nist.gov/vuln/detail/CVE-2022-47509  
Medium  

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228  
https://nvd.nist.gov/vuln/detail/CVE-2021-44228  
Critical  

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-26903  
https://nvd.nist.gov/vuln/detail/CVE-2022-26903  
Windows RCE  
High  

## Opgave 46

https://cwe.mitre.org/data/definitions/287.html  

https://cwe.mitre.org/data/definitions/272.html

https://cwe.mitre.org/data/definitions/1329.html  