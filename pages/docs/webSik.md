# Opgave 3  -  OWASP top 10 Injections

## Information
I denne øvelse skal du arbejde med [OWASP top 10 number 3 - injection](https://owasp.org/Top10/A03_2021-Injection/).
Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner

1. Læs Beskrivelsen af injection sårbarheden, og beskriv med dine egne ord hvad denne type sårbarhed dækker over.
Man skriver nogle kode som skal eksekveres i en bruger input felt f.eks. sql injection.  
2. I afsnittet _how to prevent_ bliver en af forstaltningerne mod injection anbgreb, beskrevet som _positive Server side input validation_
, hvad betyder det?  
input validation betyder at kun "gyldig" data er accepterede. Server-side er at det sker på serveren. Postitive betyder at man har en liste af input der accepteres dvs at man har en allow list i stedet for en block list. Så serveren acceptere kun gyldig data.
3. I afsnittet _List of mapped CWEs_ fremgår CWE-20, hvad beskriver denne CWE?
Improper Input Validation er at man har implementerede input validation forkerte så farlige inputs kan kokom igennem  

# Opgave 4  - OWASP top 10 Insecure design

## Information
I denne øvelse skal du arbejde med [OWASP top 10 number 4 - Insecure design](https://owasp.org/Top10/A04_2021-Insecure_Design/).
Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner
1. Hvad bliver overordnet beskrevet i afsnittet _Requirements and Resource management_, og kender du en tilgang som dækker dele af det beskrevet?
Systemmål, sikkerhedsmål.
2. Hvad menes der med Secure design?
Evaluere trussler med trusselsmodelering og være sikker på at koden er robust designede.
3. I afsnittet _Secure design_ bliver der nævnt en type modellering som bør indgår i en udviklings process. Hvilken type modellering er der her tale om?
trusselsmodelering

# Opgave 5  - ASVS

## Information
I denne øvelse skal du besvare en række spørgsmål om [OWASP Application Security Verification Standard](https://github.com/OWASP/ASVS/tree/v4.0.3#latest-stable-version---403).
(Du kan finde den seneste udgave ved at klikke på linket)
## Instruktioner
1. I hvilket afsnit kan man finde et overblik over hvad man bør sikre sig ift. input validering?
V5 Validation, Sanitization and Encoding
2. Hvad beskrives der i afsnit _V1 Architecture, Design and threat modelling_
Sikkerhed i arkitekturen
3. I nogen afsnit referes der til _NIST_ , hvad er NIST?
NIST er en samling af standarder, guidlines og best practices man kan bruge for at øge sikkerheden  
https://www.nist.gov/cybersecurity  
# Opgave 6  - Web applikation med ASP .Net Core MVC

## Information
I denne opgave skal i implementerer en web applikation der gør brug af en template engine (.Net Razor) til
at generer dynamisk HTML.

Du skal følge og implementerer [Get started with ASP.NET Core MVC](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-7.0&tabs=visual-studio-code) tutorialen.
Bemærk at strukturen er meget lig den API applikation i udarbejdet i faget _Software sikkerhed_ i mandags.

**Der er flere sider i denne tutorial, i skal lave frem til og med Add Validation**



## Instruktioner
Der kommer meget information i denne tutorial, men vær særlig opmærksom på følgende:  
- Hvad er en  controller?  
- Hvad er en View?  
En view indeholder html og andre ting som brugeren interagere med
- Hvor henne i appliaktionen laves den indledende input validering?  

## Uge 38

klon repository `https://github.com/mesn1985/HackerLab`

kør kommandoen `docker-compose -f docker-compose.yml --compatibility up -d`

# Opgave 7  - (Gruppe øvelse) crAPI system og sikkerheds mål

## Information
I denne øvelse anvender vi system og sikkerheds mål til at analyser en applikation.
Modsat tidligere øvelser i faget _software sikkerhed_ anvender vi  ikke system og sikkerheds målene til at
skabe overblik over hvordan trussels aktører kan angribe vores system. Men i stedet til hvordan vi selv 
kan misbruge et system, inden vi selv udfører en pentest. 

## Instruktioner
1. Identificer system målene for crAPI (kun web delen, mailhoq kan i ignorer).  
_Husk at identificer betyder at i blot kan nøjes udarbejde et brugstilfælde diagram hvor hvert brugstilfælde har sigende titler_  
1. Identificer sikkerheds målene for hvert system mål.
2. Identificer mulige angrebsflader ved at lave en skitse over applikations arkitekture (Tilsvarende tilgangen med trussels modellering)  
_Da det er en blackbox test, er det kun der API'er som kan identificer fra browser der kan tegnes ind på skitsen. Burp suite eller browserens udvikler værktøjer kan hjælpe med identificer de api'er som browseren sender forspøgelser til_.

## Links
[Intercepting traffic with burp suite](https://www.youtube.com/watch?v=Nr2fYpStshA)

# Opgave 8  - BOLA & Excessive data exposure.

## Information
I denne øvelse skal der arbejdes med identificering af sårbarheder.

Øvelserne er beskrevet her [crAPI - BOLA and Excessive data exposure](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md).

Formålet med øvelserne er at give en grundlæggende introduktion til BOLA sårbarheder, samt excessive data exposure.

Du kan anvende denne tutorial til at lære hvordan man analyser trafik med Burp suite [Intercepting traffic with burp suite](https://www.youtube.com/watch?v=Nr2fYpStshA)

**Det er vigtig at du noterer fremgangsmåden fra alle øvelserne ned, da du skal bruge dem til at udarbejde test cases i næste uge**

## Instruktioner
1. Udfør øvelse [1 - BOLA using object ids without authorization](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  

Bruger A  
email: email@email.com  
password: P@ssword123  

![UserAId](images/UserAId.png)  
![CarAId](images/CarAId.png)  

Bruger B  
email: email2@email.com  
password: P@ssword123  

![UserBId](images/UserBId.png)  
![CarBId](images/CarBId.png)  

2. Udfør øvelse [2 - Excessive Data exposure](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  

???
![PlainPassword](images/PlainPassword.png)  

3. Brug resultaterne fra øvelse 1 og 2 til at finde kørtøjs placeringen fra en bruger du ikke selv har oprettet.  
4. Udfør øvelse [3 - Explorer OWASP Application Security Verification standard.](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  
_Husk at notre resultatet_  
5.  Udfør øvelse [4 - Get access to other customers mechanic reports.](https://github.com/mesn1985/HackerLab/blob/main/crAPI/2_Exploiting_BOLA_And_Excessive_Data_Exposure.md)  
![UserAId](images/UserAId.png)  
![UserBId](images/UserBId.png)  
6.  (Ekstra opgave) Anvend [OWASP ZAP's](https://www.zaproxy.org/)  [Automatisk skanning](https://www.youtube.com/watch?v=wLfRz7rRsH4) og se om du kan finde yderlige sårbarheder.
7.  (Ekstra opgave) Se om du kan eksekverer flere af dine tidligere identificeret misbrugstilfælde.

# Opgave 10 - Aktiv Rekognoscering
## Information

I disse øvelser skal du arbejde med aktiv Rekognoscering. Vil anvende den teknik der kaldes path traversal eller directory attack. Tilgangen er at man anvender en liste kaldet en Wordlist, som indeholder flere forskellige ord. Hver af disse ord prøver man så af mod en web applikation, og ser hvilket http status kode den retunerer. Man anvender værktøjer til at automatisk udføre afprøvning af hvert ord. I disse øvelser vil i skulle anvende Gobuster og OWasp ZAP.

Der er en del læring i øvelserne, så bevæg dig langsomt frem. Du kommer til at skulle læse op på værktøjerne mens du udføre øvelserne.

Husk at gemme de Juiceshop og crAPI wordlister du laver løbende gennem øvelserne

Active reconnaissance is about finding the attack surfaces of a system.
In these exercises you will do reconnaissance by find scanning all the ports of
a host, and afterwards scanning for active services on the host.
Then you will enumerate crAPI to determine available url paths.

## prerequisites

The [setup of the lab](../README.md) should be completed.

The wordlists from [Seclist](https://www.kali.org/tools/seclists/) should be install
along with [Gobuster](https://www.kali.org/tools/gobuster/) on a Kali Linux instance(I use [Kali](https://www.kali.org/docs/wsl/wsl-preparations/) on WSL for convenience).

The wordlist used in these exercise can be found on the following paths:
- `/usr/share/wordlists/seclists/Discovery/Web-Content/common.txt`
- `/usr/share/wordlists/seclists/Discovery/Web-Content/quickhits.txt`

If you wish to familiarize yourself with the basics of Gobuster, you can watch this [introduction tutorial](https://www.youtube.com/watch?v=HjXNK-mYwDQ)
  
## 1 Nmap all ports
Use [NMap](https://nmap.org/) to [scan all ports](https://nmap.org/book/man-port-specification.html), and send the output to an [xml file](https://nmap.org/book/man-output.html).
This will give a complete overview of all available ports on the host, and the name of the service available on the port.

If you are using the setup provided by this repository, your output will look somewhat like this:  
![NMap Full port scan](./Images/NmapFullPortScan.jpg)

This of course provides an overview of which ports have an active service, but there is no insight 
to what the service actual is. Next exercise you will have to perform a more detailed scan.
<!-- nmap -p- 127.0.0.1 -oX allportscan -->
![NmapAllPortsXML](images/WebSik/NmapAllPortsXML.png)  

## 2 Nmap service scan
Use [NMap](https://nmap.org/) to perform a scan with the [default scripts and version detection enabled](https://explainshell.com/explain?cmd=nmap+-sC+-sV+-v+)
and output to an [xml file](https://nmap.org/book/man-output.html).
  
The output of the scan will be to big to make sense of in the console. You should use a text editor to review the
output file. For instance i use [vscode](https://code.visualstudio.com/), the setup is documented [here](https://code.visualstudio.com/docs/remote/wsl).
_If you are using Kali on WSL, and you already have vscode installed on the windows host, you can simply type code <Name of output file>_

Once you have opened the output file. Try to identify which port belongs to Juice shop, and which port belongs to crAPI.
_You know this from the docker setup. But the purpose here is to learn how to use port scans obtain information about services_

<!-- nmap -sC -sV 127.0.0.1 -oA service_scan -->
`nmap -sC -sV 127.0.0.1 -oA service_scan`  
![crapiPortXML](images/WebSik/crapiPort.png)  
![JuiceShopPortXML](images/WebSik/JuiceShopPort.png)  

## 3 Enumerating crApi with gobuster and the wordlist common.txt
In this exercise, you will use the enumeration tool [Gobuster](https://www.kali.org/tools/gobuster/) along with the wordlist `common.txt` from [SecLists](https://www.kali.org/tools/seclists/),
to discover available URL paths on crAPI.

Most likely you will encounter two error. The first one is crAPI complaining about a self-signed certificate,
you need to tell Gobuster to [Skip TLS certificate verification](https://3os.org/penetration-testing/cheatsheets/gobuster-cheatsheet/#dir-mode-options).
The error you will most likely encounter is Gobuster telling you, that crAPI returns [HTTP status code 200](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/200)
with non-existing path. Essentially meaning that code 200 OK will always be returned, so every path from the wordlist will seem valid in Gobusters scan. To circumvent this 
error you need [Exclude the length](https://hackertarget.com/gobuster-tutorial/) of the default site. This tells Gobuster to ignore response with a specific content length,
and Gobuster can therefor separate default 200 responses from valid responses. 
  
Gobuster is a very agressive scanner and therefor also very noise (Easily detected by Intrusion dection system). To make Gobuster
less noisy you could use [delay](https://hackertarget.com/gobuster-tutorial/) to set the time between each send request.

Perform the following task:
- Use [Gobuster](https://www.kali.org/tools/gobuster/) to enumerate crAPI with the wordlist common.txt. **Remember that crAPI use the HTTPS protocol**
- Create a file called `crAPI_wordlist.txt` and add the path of all the positive responses to it(status code 200's and 300's).

You are now building your own specific crAPI wordlist using the paths you know have discovered exists.

_At least one of the resulting url paths are interesting, but for now we will just focus on discovery_
![crapiGobusterCommon](images/WebSik/crapiGobuster.png)  

## 4 Enumerating crAPI with gobuster and the wordlist quickhits.txt
In this exercise you should repeat exercise 3, but use the wordlist `quickhits.txt` instead of `common.txt`.

Perform the following tasks:
- Use [Gobuster](https://www.kali.org/tools/gobuster/) to enumerate crAPI with the wordlist quickhits.txt.
- add the path of all the positive responses to it(status code 200's and 300's) to the `crAPI_wordlist.txt`.
- One of the positive responses lead to an interesting vulnerability. See if you can identify the vulnerability. (You dont have to abuse, just identify it).

Now you have your own specific wordlist for crAPI which you can execute any time. The paths on the wordlist does not lead to vulnerabilities(At least not all of them),
but i provides you with an overview of the available paths. And you even discovered a single vulnerability along the way. 

![crapiGobusterQuickhits](images/WebSik/crapiGobuster2.png)  

## 5 Enumerating crAPI with ZAP.
[OWasp ZAP](https://www.zaproxy.org/) is a open source web analysis tool. Unlike [Burp suite](https://portswigger.net/burp), you don't have to pay for the full functionality of the application.
One of ZAPs uses is automated scanning a web applications, creating a site map and detecting potential vulnerabilities. In this exercise ZAP will be used for discovery against crAPI.

If you wish to familiarize yourself with the basics of ZAP [here](https://www.zaproxy.org/getting-started/) in the section _Running an automated scan_  
  
For now there are 3 interesting outputs from the automatic scan that we will pay attention to: The Site map (in the left pane), Alerts (shown in the bottom panes), spider (Or Ajax spider, in the bottom panes).

Perform the following tasks:
- Initiate an automated scan (use Ajax spiders) and wait for the scan to complete.
- Review the alerts. Can you correlate any of the alerts to the vulnerability you discovered in exercise 4?
- What information can you find in the pane `AJAX Spider`?

Firefox virkede ikke. Har også prøvede headless  
![ZAPAjaxSpiderBrowserProblem](images/WebSik/ZAPAjaxSpiderBrowserProblem.png)  

Chrome headless virker
![ZAPAjaxSpiderWorks](images/WebSik/ZAPAjaxSpiderWorks.png)  
ZAP will proceed to crawl the web application with its spider and passively scan each page it finds. Then ZAP will use the active scanner to attack all of the discovered pages, functionality, and parameters.

ZAP provides 2 spiders for crawling web applications, you can use either or both of them from this screen.

The traditional ZAP spider which discovers links by examining the HTML in responses from the web application. This spider is fast, but it is not always effective when exploring an AJAX web application that generates links using JavaScript.

https://www.w3schools.com/whatis/whatis_ajax.asp

For AJAX applications, ZAP’s AJAX spider is likely to be more effective. This spider explores the web application by invoking browsers which then follow the links that have been generated. The AJAX spider is slower than the traditional spider and requires additional configuration for use in a “headless” environment.

ZAP will passively scan all of the requests and responses proxied through it. So far ZAP has only carried out passive scans of your web application. Passive scanning does not change responses in any way and is considered safe. Scanning is also performed in a background thread to not slow down exploration. Passive scanning is good at finding some vulnerabilities and as a way to get a feel for the basic security state of a web application and locate where more investigation may be warranted.

Active scanning, however, attempts to find other vulnerabilities by using known attacks against the selected targets. Active scanning is a real attack on those targets and can put the targets at risk, so do not use active scanning against targets you do not have permission to test.  
![ZAPSideBar](images/WebSik/ZAPSideBar.png)  
![ZAPAlarmer](images/WebSik/ZAPAlarmer.png)  
![ZAPClickedOnAlarm](images/WebSik/ZAPClickedOnAlarm.png)  